package lv.artjomsfedosenko.sonoffcontrolltest.commands;

public class AddDeviceCommand implements CommandInterface {

    private String name;

    private String actionTopic;

    private String stateTopic;

    private String telemetryTopic;

    private String deviceType;

    private String moduleId;

    public String getName() {
        return name;
    }

    public AddDeviceCommand setName(String name) {
        this.name = name;
        return this;
    }

    public String getActionTopic() {
        return actionTopic;
    }

    public AddDeviceCommand setActionTopic(String actionTopic) {
        this.actionTopic = actionTopic;
        return this;
    }

    public String getStateTopic() {
        return stateTopic;
    }

    public AddDeviceCommand setStateTopic(String stateTopic) {
        this.stateTopic = stateTopic;
        return this;
    }

    public String getTelemetryTopic() {
        return telemetryTopic;
    }

    public AddDeviceCommand setTelemetryTopic(String telemetryTopic) {
        this.telemetryTopic = telemetryTopic;
        return this;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public AddDeviceCommand setDeviceType(String deviceType) {
        this.deviceType = deviceType;
        return this;
    }

    public String getModuleId() {
        return moduleId;
    }

    public AddDeviceCommand setModuleId(String moduleId) {
        this.moduleId = moduleId;
        return this;
    }
}

package lv.artjomsfedosenko.sonoffcontrolltest.commands;

import lv.artjomsfedosenko.sonoffcontrolltest.domain.DeviceConfiguration;
import lv.artjomsfedosenko.sonoffcontrolltest.domain.DeviceTypes;
import lv.artjomsfedosenko.sonoffcontrolltest.domain.devices.Device;
import lv.artjomsfedosenko.sonoffcontrolltest.services.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AddDeviceCommandHandler
        implements CommandHandlerInterface<AddDeviceCommand, AddDeviceResult> {

    private static Logger logger = LogManager.getLogger(AddDeviceCommandHandler.class);

    private DeviceConfigurationStorage deviceConfigurationStorage;

    private DeviceLoader deviceLoader;

    private DeviceConfigurationValidator deviceConfigurationValidator;

    @Autowired
    public AddDeviceCommandHandler(
            DeviceConfigurationStorage deviceConfigurationStorage,
            DeviceConfigurationValidator deviceConfigurationValidator,
            DeviceLoader deviceLoader
    ) {
        this.deviceConfigurationStorage = deviceConfigurationStorage;
        this.deviceConfigurationValidator = deviceConfigurationValidator;
        this.deviceLoader = deviceLoader;
    }

    @Override
    public AddDeviceResult execute(AddDeviceCommand command) {

        logger.info("Adding device: " + command);
        //Create Configuration object
        DeviceConfiguration configuration = new DeviceConfiguration()
                .setName(command.getName())
                .setActionTopic(command.getActionTopic())
                .setStateTopic(command.getStateTopic())
                //.setStatusTopic(command.getStatusTopic())
                .setTelemetryTopic(command.getTelemetryTopic())
                .setDeviceType(DeviceTypes.valueOf(command.getDeviceType()))
                .setModuleId(command.getModuleId());

        logger.info("Created configuration: " + configuration);
        //Validate data
        this.deviceConfigurationValidator.validate(configuration);
        logger.info("Configuration validation passed");

        //Save Device Configuration
        try {
            Integer deviceId = this.deviceConfigurationStorage.save(configuration);
            configuration.setId(deviceId);
        } catch (Exception e) {
            //TODO: implement reasonable and understandable Exception hierarchy for core commands
            logger.warn("Exception while saving device configuration");
            logger.warn(e.getMessage());
            logger.warn(e.getStackTrace());
            throw new IllegalArgumentException();
        }

        //Create a Device object
        //TODO: handle exception properly
        Device device = this.deviceLoader.loadDevice(configuration);

        return new AddDeviceResult(device.getId());
    }

    @Override
    public Class getCommandType() {
        return AddDeviceCommand.class;
    }
}

package lv.artjomsfedosenko.sonoffcontrolltest.commands;

public class AddDeviceResult implements CommandResultInterface {

    //Should contain added device Id ?

    private Integer deviceId;

    public AddDeviceResult(Integer deviceId) {
        this.deviceId = deviceId;
    }

    public Integer getDeviceId() {
        return deviceId;
    }
}

package lv.artjomsfedosenko.sonoffcontrolltest.commands;

public interface CommandHandlerInterface<C extends CommandInterface, R extends CommandResultInterface> {

    R execute(C command);

    Class getCommandType();

}

package lv.artjomsfedosenko.sonoffcontrolltest.commands;

import lv.artjomsfedosenko.sonoffcontrolltest.domain.devices.Device;
import lv.artjomsfedosenko.sonoffcontrolltest.domain.devices.Switch;
import lv.artjomsfedosenko.sonoffcontrolltest.domain.devices.TemperatureHumiditySensor;
import lv.artjomsfedosenko.sonoffcontrolltest.domain.devices.TemperatureSensor;
import lv.artjomsfedosenko.sonoffcontrolltest.dto.DeviceDTO;
import lv.artjomsfedosenko.sonoffcontrolltest.dto.SwitchDTO;
import lv.artjomsfedosenko.sonoffcontrolltest.dto.TemperatureHumiditySensorDTO;
import lv.artjomsfedosenko.sonoffcontrolltest.dto.TemperatureSensorDTO;
import lv.artjomsfedosenko.sonoffcontrolltest.services.DeviceRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class ListDeviceCommandHandler
        implements CommandHandlerInterface<ListDevicesCommand, ListDevicesResult> {

    private DeviceRegistry deviceRegistry;

    @Autowired
    public ListDeviceCommandHandler(DeviceRegistry deviceRegistry) {
        this.deviceRegistry = deviceRegistry;
    }

    @Override
    public ListDevicesResult execute(ListDevicesCommand command) {

        //Get devices from DeviceRegistry
        Map<Integer, Device> deviceMap = this.deviceRegistry.getDevices();

        //Convert to DTOs
        List<DeviceDTO> deviceDTOS = new ArrayList<>();
        deviceMap.forEach((deviceId, device) -> {
            DeviceDTO dto = this.convertDevice(device);
            deviceDTOS.add(dto);
        });

        ListDevicesResult result = new ListDevicesResult(deviceDTOS);

        return result;
    }

    @Override
    public Class getCommandType() {
        return ListDevicesCommand.class;
    }

    private DeviceDTO convertDevice(Device device) {

        DeviceDTO dto = null;
        if (device instanceof Switch) {
            Switch switchDevice = (Switch) device;
            dto = new SwitchDTO();
            this.fillSwitchDto((SwitchDTO) dto, switchDevice);
        }

        if (device instanceof TemperatureSensor) {
            TemperatureSensor sensorDevice = (TemperatureSensor) device;
            dto = new TemperatureSensorDTO();
            this.fillTemperatureSensorDto((TemperatureSensorDTO) dto, sensorDevice);
        }

        if (device instanceof TemperatureHumiditySensor) {
            TemperatureHumiditySensor humiditySensor = (TemperatureHumiditySensor) device;
            dto = new TemperatureHumiditySensorDTO();
            this.fillTemperatureHumiditySensorDto(
                    (TemperatureHumiditySensorDTO) dto, humiditySensor
            );
        }

        return dto;
    }

    private void fillSwitchDto(SwitchDTO dto, Switch device) {

        dto.setId(device.getId())
            .setName(device.getName())
            .setDeviceStatus(device.getDeviceStatus().toString());
        dto.setSwitchPosition(device.getSwitchPosition().toString())
                .setSwitchState(device.getSwitchState().toString());

    }

    private void fillTemperatureSensorDto(TemperatureSensorDTO dto, TemperatureSensor device) {

        dto.setId(device.getId())
                .setName(device.getName())
                .setDeviceStatus(device.getDeviceStatus().toString());
        dto.setTemperature(device.getTemperature());
    }

    private void fillTemperatureHumiditySensorDto(
            TemperatureHumiditySensorDTO dto, TemperatureHumiditySensor device
    ) {
        dto.setId(device.getId())
                .setName(device.getName())
                .setDeviceStatus(device.getDeviceStatus().toString());
        dto.setTemperature(device.getTemperature())
                .setHumidity(device.getHumidity());
    }
}

package lv.artjomsfedosenko.sonoffcontrolltest.commands;

import lv.artjomsfedosenko.sonoffcontrolltest.dto.DeviceDTO;

import java.util.List;

public class ListDevicesResult implements CommandResultInterface {

    private List<DeviceDTO> devices;

    public ListDevicesResult(List<DeviceDTO> devices) {
        this.devices = devices;
    }

    public List<DeviceDTO> getDevices() {
        return devices;
    }
}

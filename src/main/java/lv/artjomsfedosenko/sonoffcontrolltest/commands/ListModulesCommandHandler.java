package lv.artjomsfedosenko.sonoffcontrolltest.commands;

import lv.artjomsfedosenko.sonoffcontrolltest.domain.Module;
import lv.artjomsfedosenko.sonoffcontrolltest.dto.ModuleDTO;
import lv.artjomsfedosenko.sonoffcontrolltest.mqtt.ModuleWatcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class ListModulesCommandHandler
        implements CommandHandlerInterface<ListModulesCommand, ListModulesResult> {

    private ModuleWatcher moduleWatcher;

    @Autowired
    public ListModulesCommandHandler(ModuleWatcher moduleWatcher) {
        this.moduleWatcher = moduleWatcher;
    }

    @Override
    public ListModulesResult execute(ListModulesCommand command) {

        Map<String, Module> modules = this.moduleWatcher.getModules();

        List<ModuleDTO> moduleList = new ArrayList<>();
        for (Module module : modules.values()) {

            ModuleDTO moduleDTO = new ModuleDTO(
                    module.getModuleId(),
                    module.getModuleStatus().toString(),
                    new ArrayList<>(module.getSubscribedTopics())
            );

            moduleList.add(moduleDTO);
        }

        return new ListModulesResult(moduleList);
    }

    @Override
    public Class getCommandType() {
        return ListModulesCommand.class;
    }
}

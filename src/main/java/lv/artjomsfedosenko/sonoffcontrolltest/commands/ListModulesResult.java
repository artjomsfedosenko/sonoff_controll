package lv.artjomsfedosenko.sonoffcontrolltest.commands;

import lv.artjomsfedosenko.sonoffcontrolltest.domain.Module;
import lv.artjomsfedosenko.sonoffcontrolltest.dto.ModuleDTO;

import java.util.List;
import java.util.Map;

public class ListModulesResult implements CommandResultInterface {

    private List<ModuleDTO> modules;

    public ListModulesResult(List<ModuleDTO> modules) {
        this.modules = modules;
    }

    public List<ModuleDTO> getModules() {
        return modules;
    }
}

package lv.artjomsfedosenko.sonoffcontrolltest.commands;

public class SwitchOffCommand implements CommandInterface {

    private Integer switchId;

    public SwitchOffCommand(Integer switchId) {
        this.switchId = switchId;
    }

    public Integer getSwitchId() {
        return switchId;
    }
}

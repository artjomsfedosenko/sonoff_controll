package lv.artjomsfedosenko.sonoffcontrolltest.commands;

public class SwitchOnCommand implements CommandInterface {

    private Integer switchId;

    public SwitchOnCommand(Integer switchId) {
        this.switchId = switchId;
    }

    public Integer getSwitchId() {
        return switchId;
    }
}

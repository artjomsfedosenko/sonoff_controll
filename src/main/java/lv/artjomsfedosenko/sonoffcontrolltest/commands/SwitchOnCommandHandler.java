package lv.artjomsfedosenko.sonoffcontrolltest.commands;

import lv.artjomsfedosenko.sonoffcontrolltest.domain.devices.Device;
import lv.artjomsfedosenko.sonoffcontrolltest.mqtt.Broker;
import lv.artjomsfedosenko.sonoffcontrolltest.services.DeviceRegistry;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SwitchOnCommandHandler
        implements CommandHandlerInterface<SwitchOnCommand, SwitchOnResult> {

    private static Logger logger = LogManager.getLogger(SwitchOnCommandHandler.class);

    private DeviceRegistry deviceRegistry;

    private Broker broker;

    @Autowired
    public SwitchOnCommandHandler(DeviceRegistry deviceRegistry, Broker broker) {
        this.deviceRegistry = deviceRegistry    ;
        this.broker = broker;
    }

    @Override
    public SwitchOnResult execute(SwitchOnCommand command) {

        logger.info("Executing Switch ON command for device: " + command.getSwitchId());
        Integer switchId = command.getSwitchId();
        Device device = this.deviceRegistry.getDeviceById(switchId);

        if (device == null) {
            logger.warn("Device with id: " + switchId + " does not exist");
            throw new IllegalArgumentException("Device with id: " + switchId + " does not exist");
        }

        String actionTopic = device.getActionTopic();
        this.broker.publish(actionTopic, "ON");

        return new SwitchOnResult();
    }

    @Override
    public Class getCommandType() {
        return SwitchOnCommand.class;
    }
}

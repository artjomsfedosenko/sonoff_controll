package lv.artjomsfedosenko.sonoffcontrolltest.controller;

import lv.artjomsfedosenko.sonoffcontrolltest.commands.*;
import lv.artjomsfedosenko.sonoffcontrolltest.dto.DeviceFormDTO;
import lv.artjomsfedosenko.sonoffcontrolltest.services.CommandExecutor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/device")
public class DeviceController {

    private static final Logger logger = LogManager.getLogger(DeviceController.class);

    @Autowired
    private CommandExecutor commandExecutor;

    @RequestMapping("/list")
    public String listDevices(Model model){

        //ListDevicesResult result = null;
        ListDevicesResult result = this.commandExecutor
                .execute(new ListDevicesCommand());

        logger.info("Devices in DeviceController: " + result.getDevices().size());

        model.addAttribute("devices", result.getDevices());

        return "list_devices";
    }

    @RequestMapping(value = "/switchon/{switchId}")
    public ModelAndView switchOn(@PathVariable("switchId") int switchId) {

        logger.info("Executing switch ON for switch: " + switchId);

        SwitchOnCommand command = new SwitchOnCommand(switchId);
        this.commandExecutor.execute(command);

        return new ModelAndView("redirect:/device/list");
    }

    @RequestMapping(value = "/switchoff/{switchId}")
    public ModelAndView switchOff(@PathVariable("switchId") int switchId) {

        logger.info("Executing switch OFF for switch: " + switchId);

        SwitchOffCommand command = new SwitchOffCommand(switchId);
        this.commandExecutor.execute(command);

        return new ModelAndView("redirect:/device/list");
    }

    @GetMapping("/add")
    public String showAddDeviceForm(Model model) {

        model.addAttribute("device", new DeviceFormDTO());

        System.out.println("Show device form");

        return "device_form";
    }

    @PostMapping("/add")
    public String addDevice(@ModelAttribute DeviceFormDTO deviceData, Model model) {

        System.out.println("Device data received from form: " + deviceData);

        AddDeviceCommand command = new AddDeviceCommand()
                .setName(deviceData.getName())
                .setActionTopic(deviceData.getActionTopic())
                .setStateTopic(deviceData.getStateTopic())
                .setTelemetryTopic(deviceData.getTelemetryTopic())
                .setDeviceType(deviceData.getDeviceType())
                .setModuleId(deviceData.getModuleId());

        //TODO: use built in Exception Handler
        try {
            AddDeviceResult result =this.commandExecutor.execute(command);
        } catch (Exception e) {
            //Process result
        }

        model.addAttribute("device", deviceData);

        return "device_form";
    }
}

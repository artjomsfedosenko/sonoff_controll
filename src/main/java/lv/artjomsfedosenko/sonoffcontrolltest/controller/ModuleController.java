package lv.artjomsfedosenko.sonoffcontrolltest.controller;

import lv.artjomsfedosenko.sonoffcontrolltest.commands.ListModulesCommand;
import lv.artjomsfedosenko.sonoffcontrolltest.commands.ListModulesResult;
import lv.artjomsfedosenko.sonoffcontrolltest.domain.Module;
import lv.artjomsfedosenko.sonoffcontrolltest.dto.ModuleDTO;
import lv.artjomsfedosenko.sonoffcontrolltest.services.CommandExecutor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/module")
public class ModuleController {

    //private static Logger logger = LogManager.getLogger(Log4jExample.class);

    private static final Logger logger = LogManager.getLogger(ModuleController.class);

    @Autowired
    private CommandExecutor commandExecutor;

    @RequestMapping("/list")
    public String listSwitches(Model model) {

        ListModulesResult result = null;
        try {
            result = commandExecutor.execute(new ListModulesCommand());
        } catch (Exception e) {
            //TODO: handle exception
        }
        List<ModuleDTO> modules = result.getModules();

        logger.info("Modules in controller from discoverer");
        modules.forEach(module -> logger.info(module));


        model.addAttribute("modules", modules);
        //model.addAttribute("switch", "This is SW");

        return "module_list";
    }
}

package lv.artjomsfedosenko.sonoffcontrolltest.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/switch")
public class SwitchController {



    @RequestMapping("/list")
    public String listSwitches(Model model) {

        System.out.println("In controller");


        System.out.println("Devices in controller from discoverer");

        model.addAttribute("switch", "This is SW");

        return "switch_list";
    }

    @RequestMapping("/toggle")
    public String toggleSwitch() {

        return "switch_list";
    }
}

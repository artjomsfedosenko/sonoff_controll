package lv.artjomsfedosenko.sonoffcontrolltest.domain;

import java.util.Objects;

public class DeviceConfiguration {

    private Integer id;

    private String name;

    private String actionTopic;

    private String stateTopic;

    private String telemetryTopic;

    private DeviceTypes deviceType;

    private String moduleId;

    public Integer getId() {
        return id;
    }

    public DeviceConfiguration setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public DeviceConfiguration setName(String name) {
        this.name = name;
        return this;
    }

    public String getActionTopic() {
        return actionTopic;
    }

    public DeviceConfiguration setActionTopic(String actionTopic) {
        this.actionTopic = actionTopic;
        return this;
    }

    public String getStateTopic() {
        return stateTopic;
    }

    public DeviceConfiguration setStateTopic(String stateTopic) {
        this.stateTopic = stateTopic;
        return this;
    }

    public String getTelemetryTopic() {
        return telemetryTopic;
    }

    public DeviceConfiguration setTelemetryTopic(String telemetryTopic) {
        this.telemetryTopic = telemetryTopic;
        return this;
    }

    public DeviceTypes getDeviceType() {
        return deviceType;
    }

    public DeviceConfiguration setDeviceType(DeviceTypes deviceType) {
        this.deviceType = deviceType;
        return this;
    }

    public String getModuleId() {
        return moduleId;
    }

    public DeviceConfiguration setModuleId(String moduleId) {
        this.moduleId = moduleId;
        return this;
    }

    @Override
    public String toString() {
        return "DeviceConfiguration{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", actionTopic='" + actionTopic + '\'' +
                ", stateTopic='" + stateTopic + '\'' +
                ", telemetryTopic='" + telemetryTopic + '\'' +
                ", deviceType=" + deviceType +
                ", moduleId='" + moduleId + '\'' +
                '}';
    }
}

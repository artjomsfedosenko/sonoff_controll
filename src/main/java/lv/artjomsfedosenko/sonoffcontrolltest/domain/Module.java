package lv.artjomsfedosenko.sonoffcontrolltest.domain;

import java.util.HashSet;
import java.util.Set;

public class Module {

    private String moduleId;

    private ModuleStatus moduleStatus;

    private Set<String> subscribedTopics =  new HashSet<>();

    public Module(String moduleId, ModuleStatus moduleStatus) {
        this.moduleId = moduleId;
        this.moduleStatus = moduleStatus;
    }

    public String getModuleId() {
        return moduleId;
    }

    public void setModuleId(String moduleId) {
        this.moduleId = moduleId;
    }

    public Set<String> getSubscribedTopics() {
        return subscribedTopics;
    }

    public void setSubscribedTopics(Set<String> subscribedTopics) {
        this.subscribedTopics = subscribedTopics;
    }

    public ModuleStatus getModuleStatus() {
        return moduleStatus;
    }

    public void setModuleStatus(ModuleStatus moduleStatus) {
        this.moduleStatus = moduleStatus;
    }

    public void addSubscribedTopic(String topic) {
        this.subscribedTopics.add(topic);
    }

    public void removeSubscribedTopic(String topic) {
        this.subscribedTopics.remove(topic);
    }

    @Override
    public String toString() {
        return "Module{" +
                "moduleId='" + moduleId + '\'' +
                ", moduleStatus=" + moduleStatus +
                ", subscribedTopics=" + subscribedTopics +
                '}';
    }
}

package lv.artjomsfedosenko.sonoffcontrolltest.domain;

public enum ModuleStatus {
    CONNECTED, DISCONNECTED, CONNECTION_LOST
}

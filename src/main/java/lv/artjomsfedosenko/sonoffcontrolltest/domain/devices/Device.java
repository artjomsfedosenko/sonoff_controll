package lv.artjomsfedosenko.sonoffcontrolltest.domain.devices;


public class Device {

    private Integer id;

    private String name;

    private String actionTopic;

    private String stateTopic;

    private String telemetryTopic;

    private String moduleId;

    private DeviceStatus deviceStatus;

    public Integer getId() {
        return id;
    }

    public Device setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Device setName(String name) {
        this.name = name;
        return this;
    }

    public String getActionTopic() {
        return actionTopic;
    }

    public Device setActionTopic(String actionTopic) {
        this.actionTopic = actionTopic;
        return this;
    }

    public String getStateTopic() {
        return stateTopic;
    }

    public Device setStateTopic(String stateTopic) {
        this.stateTopic = stateTopic;
        return this;
    }

    public String getTelemetryTopic() {
        return telemetryTopic;
    }

    public Device setTelemetryTopic(String telemetryTopic) {
        this.telemetryTopic = telemetryTopic;
        return this;
    }

    public String getModuleId() {
        return moduleId;
    }

    public Device setModuleId(String moduleId) {
        this.moduleId = moduleId;
        return this;
    }

    public DeviceStatus getDeviceStatus() {
        return deviceStatus;
    }

    public Device setDeviceStatus(DeviceStatus deviceStatus) {
        this.deviceStatus = deviceStatus;
        return this;
    }

    @Override
    public String toString() {
        return "Device{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", actionTopic='" + actionTopic + '\'' +
                ", stateTopic='" + stateTopic + '\'' +
                ", telemetryTopic='" + telemetryTopic + '\'' +
                ", moduleId='" + moduleId + '\'' +
                ", deviceStatus=" + deviceStatus +
                '}';
    }
}

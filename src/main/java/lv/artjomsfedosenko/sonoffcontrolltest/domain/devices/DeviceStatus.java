package lv.artjomsfedosenko.sonoffcontrolltest.domain.devices;

public enum DeviceStatus {
    ONLINE, OFFLINE
}

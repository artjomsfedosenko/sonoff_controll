package lv.artjomsfedosenko.sonoffcontrolltest.domain.devices;

public class Switch extends Device {

    //PowerState
    private SwitchState switchState;

    //SwitchPosition
    private SwitchPosition switchPosition;

    public SwitchState getSwitchState() {
        return switchState;
    }

    public Switch setSwitchState(SwitchState switchState) {
        this.switchState = switchState;
        return this;
    }

    public SwitchPosition getSwitchPosition() {
        return switchPosition;
    }

    public Switch setSwitchPosition(SwitchPosition switchPosition) {
        this.switchPosition = switchPosition;
        return this;
    }
}

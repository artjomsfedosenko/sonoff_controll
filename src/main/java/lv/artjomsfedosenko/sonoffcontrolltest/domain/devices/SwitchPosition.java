package lv.artjomsfedosenko.sonoffcontrolltest.domain.devices;

public enum SwitchPosition {
    ON, OFF
}

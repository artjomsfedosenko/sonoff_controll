package lv.artjomsfedosenko.sonoffcontrolltest.domain.devices;

public enum  SwitchState {
    ON, OFF
}

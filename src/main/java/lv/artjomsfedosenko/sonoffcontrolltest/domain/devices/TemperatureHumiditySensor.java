package lv.artjomsfedosenko.sonoffcontrolltest.domain.devices;

public class TemperatureHumiditySensor extends Device {

    //Temperature
    private Float temperature;

    //Humidity
    private Float humidity;

    public Float getTemperature() {
        return temperature;
    }

    public TemperatureHumiditySensor setTemperature(Float temperature) {
        this.temperature = temperature;
        return this;
    }

    public Float getHumidity() {
        return humidity;
    }

    public TemperatureHumiditySensor setHumidity(Float humidity) {
        this.humidity = humidity;
        return this;
    }
}

package lv.artjomsfedosenko.sonoffcontrolltest.domain.devices;

public class TemperatureSensor extends Device {

    //Temperature
    private Float temperature;

    //Units ???


    public Float getTemperature() {
        return temperature;
    }

    public TemperatureSensor setTemperature(Float temperature) {
        this.temperature = temperature;
        return this;
    }
}

package lv.artjomsfedosenko.sonoffcontrolltest.dto;

public class DeviceDTO {

    private Integer id;

    private String name;

    private String deviceStatus;

    public Integer getId() {
        return id;
    }

    public DeviceDTO setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public DeviceDTO setName(String name) {
        this.name = name;
        return this;
    }

    public String getDeviceStatus() {
        return deviceStatus;
    }

    public DeviceDTO setDeviceStatus(String deviceStatus) {
        this.deviceStatus = deviceStatus;
        return this;
    }
}

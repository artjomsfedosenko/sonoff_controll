package lv.artjomsfedosenko.sonoffcontrolltest.dto;

public class DeviceFormDTO {

    private String name;

    private String actionTopic;

    private String stateTopic;

    private String telemetryTopic;

    private String deviceType;

    private String moduleId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getActionTopic() {
        return actionTopic;
    }

    public void setActionTopic(String actionTopic) {
        this.actionTopic = actionTopic;
    }

    public String getStateTopic() {
        return stateTopic;
    }

    public void setStateTopic(String stateTopic) {
        this.stateTopic = stateTopic;
    }

    public String getTelemetryTopic() {
        return telemetryTopic;
    }

    public void setTelemetryTopic(String telemetryTopic) {
        this.telemetryTopic = telemetryTopic;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getModuleId() {
        return moduleId;
    }

    public void setModuleId(String moduleId) {
        this.moduleId = moduleId;
    }
}

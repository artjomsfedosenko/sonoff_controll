package lv.artjomsfedosenko.sonoffcontrolltest.dto;

import java.util.List;
import java.util.Set;

public class ModuleDTO {

    private String moduleId;

    private String moduleState;

    private List<String> subscribedTopics;

    public ModuleDTO(String moduleId, String moduleState, List<String> subscribedTopics) {
        this.moduleId = moduleId;
        this.moduleState = moduleState;
        this.subscribedTopics = subscribedTopics;
    }

    public String getModuleId() {
        return moduleId;
    }

    public String getModuleState() {
        return moduleState;
    }

    public List<String> getSubscribedTopics() {
        return subscribedTopics;
    }
}

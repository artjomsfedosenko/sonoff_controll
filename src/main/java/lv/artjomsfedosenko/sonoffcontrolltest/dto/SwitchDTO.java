package lv.artjomsfedosenko.sonoffcontrolltest.dto;

public class SwitchDTO extends DeviceDTO {

    private String switchState;

    private String switchPosition;

    public String getSwitchState() {
        return switchState;
    }

    public SwitchDTO setSwitchState(String switchState) {
        this.switchState = switchState;
        return this;
    }

    public String getSwitchPosition() {
        return switchPosition;
    }

    public SwitchDTO setSwitchPosition(String switchPosition) {
        this.switchPosition = switchPosition;
        return this;
    }
}

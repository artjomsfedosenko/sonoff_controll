package lv.artjomsfedosenko.sonoffcontrolltest.dto;

public class TemperatureHumiditySensorDTO extends DeviceDTO {

    private Float temperature;

    private Float humidity;

    public Float getTemperature() {
        return temperature;
    }

    public TemperatureHumiditySensorDTO setTemperature(Float temperature) {
        this.temperature = temperature;
        return this;
    }

    public Float getHumidity() {
        return humidity;
    }

    public TemperatureHumiditySensorDTO setHumidity(Float humidity) {
        this.humidity = humidity;
        return this;
    }
}

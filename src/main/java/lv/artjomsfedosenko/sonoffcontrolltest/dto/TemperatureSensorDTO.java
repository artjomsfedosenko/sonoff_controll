package lv.artjomsfedosenko.sonoffcontrolltest.dto;

public class TemperatureSensorDTO extends DeviceDTO {

    private Float temperature;

    public Float getTemperature() {
        return temperature;
    }

    public TemperatureSensorDTO setTemperature(Float temperature) {
        this.temperature = temperature;
        return this;
    }
}

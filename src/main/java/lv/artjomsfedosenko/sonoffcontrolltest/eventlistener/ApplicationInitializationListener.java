package lv.artjomsfedosenko.sonoffcontrolltest.eventlistener;

import lv.artjomsfedosenko.sonoffcontrolltest.domain.DeviceConfiguration;
import lv.artjomsfedosenko.sonoffcontrolltest.domain.devices.Device;
import lv.artjomsfedosenko.sonoffcontrolltest.services.DeviceConfigurationStorage;
import lv.artjomsfedosenko.sonoffcontrolltest.services.DeviceLoader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ApplicationInitializationListener implements ApplicationListener<ContextRefreshedEvent> {

    private static Logger logger = LogManager.getLogger(ApplicationInitializationListener.class);

    private DeviceConfigurationStorage configurationStorage;

    private DeviceLoader deviceLoader;

    public ApplicationInitializationListener(DeviceConfigurationStorage configurationStorage, DeviceLoader deviceLoader) {
        this.configurationStorage = configurationStorage;
        this.deviceLoader = deviceLoader;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {

        logger.info("Starting device initialisation sequence");
        //Get all configs from Storage
        List<DeviceConfiguration> configurationList = new ArrayList<>();

        try {
            configurationList = this.configurationStorage.getDeviceConfigurations();
        } catch (Exception e) {
            logger.warn("Could not load configs from storage: " + e.getMessage());
        }

        //Load each device
        for (DeviceConfiguration configuration : configurationList) {
            Device device = this.deviceLoader.loadDevice(configuration);
            logger.info("Device: " + device.getId() + " loaded");
        }
    }
}

package lv.artjomsfedosenko.sonoffcontrolltest.eventlistener;

import lv.artjomsfedosenko.sonoffcontrolltest.domain.Module;
import lv.artjomsfedosenko.sonoffcontrolltest.domain.ModuleStatus;
import lv.artjomsfedosenko.sonoffcontrolltest.domain.devices.DeviceStatus;
import lv.artjomsfedosenko.sonoffcontrolltest.events.DeviceAddedEvent;
import lv.artjomsfedosenko.sonoffcontrolltest.mqtt.ModuleWatcher;
import lv.artjomsfedosenko.sonoffcontrolltest.services.DeviceRegistry;
import lv.artjomsfedosenko.sonoffcontrolltest.services.ModuleStatusConverter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class DeviceAddedListener implements ApplicationListener<DeviceAddedEvent> {

    private static Logger logger = LogManager.getLogger(DeviceAddedListener.class);

    private ModuleWatcher moduleWatcher;

    private DeviceRegistry deviceRegistry;

    private ModuleStatusConverter converter;;

    public DeviceAddedListener(
            ModuleWatcher moduleWatcher,
            DeviceRegistry deviceRegistry,
            ModuleStatusConverter converter
    ) {
        this.moduleWatcher = moduleWatcher;
        this.deviceRegistry = deviceRegistry;
        this.converter = converter;
    }

    @Override
    public void onApplicationEvent(DeviceAddedEvent event) {

        //Get device module id
        Integer deviceId = event.getDeviceId();
        logger.info("DeviceAdded event received for device: " + deviceId);

        String moduleId = this.deviceRegistry.getDeviceModuleId(deviceId);
        logger.info("Module id: " + moduleId + " retrieved");

        //Get module
        Module module = this.moduleWatcher.getModules().get(moduleId);

        if (module != null) {
            //Get module Status
            ModuleStatus moduleStatus = module.getModuleStatus();
            logger.info("Module status is: " + moduleStatus.toString());

            //Transfer Module State to Device State
            DeviceStatus deviceStatus = this.converter.getDeviceStatusByModuleStatus(moduleStatus);
            logger.info(moduleStatus + " converted to " + deviceStatus);

            //Set Device State
            this.deviceRegistry.setDeviceStatus(deviceId, deviceStatus);
        }
    }
}


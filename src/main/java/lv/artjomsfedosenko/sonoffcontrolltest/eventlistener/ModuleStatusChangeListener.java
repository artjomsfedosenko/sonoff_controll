package lv.artjomsfedosenko.sonoffcontrolltest.eventlistener;

import lv.artjomsfedosenko.sonoffcontrolltest.domain.devices.DeviceStatus;
import lv.artjomsfedosenko.sonoffcontrolltest.events.ModuleStatusChangeEvent;
import lv.artjomsfedosenko.sonoffcontrolltest.services.DeviceRegistry;
import lv.artjomsfedosenko.sonoffcontrolltest.services.ModuleStatusConverter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class ModuleStatusChangeListener implements ApplicationListener<ModuleStatusChangeEvent> {

    private static Logger logger = LogManager.getLogger(ModuleStatusChangeListener.class);

    private DeviceRegistry deviceRegistry;

    private ModuleStatusConverter converter;

    public ModuleStatusChangeListener(DeviceRegistry deviceRegistry, ModuleStatusConverter converter) {
        this.deviceRegistry = deviceRegistry;
        this.converter = converter;
    }

    @Override
    public void onApplicationEvent(ModuleStatusChangeEvent event) {

        logger.info(
                "Module Status Change event received. Module Id: " + event.getModuleId() + ", status: " + event.getModuleStatus()
        );
        //Convert status
        DeviceStatus deviceStatus = this.converter.getDeviceStatusByModuleStatus(event.getModuleStatus());
        logger.info("Converted module status to: " + deviceStatus);

        //CAll device registry method
        this.deviceRegistry.setDeviceStatusByModuleId(
                event.getModuleId(), deviceStatus
        );
    }
}

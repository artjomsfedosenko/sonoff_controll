package lv.artjomsfedosenko.sonoffcontrolltest.events;

import org.springframework.context.ApplicationEvent;

public class DeviceAddedEvent extends ApplicationEvent {

    private Integer deviceId;

    public DeviceAddedEvent(Object source, Integer deviceId) {
        super(source);
        this.deviceId = deviceId;
    }

    public Integer getDeviceId() {
        return this.deviceId;
    }
}

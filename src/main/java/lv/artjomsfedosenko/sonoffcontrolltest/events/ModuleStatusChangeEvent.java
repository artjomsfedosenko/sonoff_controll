package lv.artjomsfedosenko.sonoffcontrolltest.events;

import lv.artjomsfedosenko.sonoffcontrolltest.domain.ModuleStatus;
import org.springframework.context.ApplicationEvent;

public class ModuleStatusChangeEvent extends ApplicationEvent {

    private String moduleId;

    private ModuleStatus moduleStatus;

    public ModuleStatusChangeEvent(Object source, String moduleId, ModuleStatus moduleStatus) {
        super(source);
        this.moduleId = moduleId;
        this.moduleStatus = moduleStatus;
    }

    public String getModuleId() {
        return moduleId;
    }

    public ModuleStatus getModuleStatus() {
        return moduleStatus;
    }
}

package lv.artjomsfedosenko.sonoffcontrolltest.mqtt;

import io.moquette.interception.AbstractInterceptHandler;
import io.moquette.server.Server;
import io.netty.buffer.Unpooled;
import io.netty.handler.codec.mqtt.MqttMessageBuilders;
import io.netty.handler.codec.mqtt.MqttPublishMessage;
import io.netty.handler.codec.mqtt.MqttQoS;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;

import static java.nio.charset.StandardCharsets.UTF_8;

@Component
public class Broker {

    private static Logger logger = LogManager.getLogger(Broker.class);
    private Server server;
    private List<? extends AbstractInterceptHandler> interceptors;

    @Autowired
    public Broker(Server server, List<? extends AbstractInterceptHandler> interceptors) {
        this.server = server;
        this.interceptors = interceptors;
    }

    @PostConstruct
    public void init() {

        for (AbstractInterceptHandler interceptor : this.interceptors) {
            logger.info("Adding interceptor: " + interceptor.getClass());
            this.server.addInterceptHandler(interceptor);
        }
    }

    public void publish(String topic, String message) {

        MqttPublishMessage mqttMessage = MqttMessageBuilders.publish()
                .topicName(topic)
                .retained(true)
                .qos(MqttQoS.AT_LEAST_ONCE)
                .payload(Unpooled.copiedBuffer(message.getBytes(UTF_8)))
                .build();

        server.internalPublish(mqttMessage, Broker.class.toString());
    }
}

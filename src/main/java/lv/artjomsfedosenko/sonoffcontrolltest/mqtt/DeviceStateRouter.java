package lv.artjomsfedosenko.sonoffcontrolltest.mqtt;

import io.moquette.interception.AbstractInterceptHandler;
import io.moquette.interception.messages.InterceptPublishMessage;
import io.netty.buffer.ByteBufUtil;
import lv.artjomsfedosenko.sonoffcontrolltest.domain.devices.Device;
import lv.artjomsfedosenko.sonoffcontrolltest.services.DeviceRegistry;
import lv.artjomsfedosenko.sonoffcontrolltest.services.devicestate.DeviceStateResolver;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

@Component
public class DeviceStateRouter extends AbstractInterceptHandler {

    private static Logger logger = LogManager.getLogger(DeviceStateRouter.class);

    //Topic, DeviceId
    private Map<String, Integer> topicRoutes = new HashMap<>();

    private DeviceRegistry registry;

    private DeviceStateResolver resolver;

    @Autowired
    public DeviceStateRouter(DeviceRegistry registry, DeviceStateResolver resolver) {
        this.registry = registry;
        this.resolver = resolver;

    }

    @Override
    public String getID() {
        return "DeviceStateRouter";
    }

    @Override
    public void onPublish(InterceptPublishMessage msg) {

        //Get topic from intercepted message
        String decodedPayload = new String(ByteBufUtil.getBytes(msg.getPayload()), Charset.forName("UTF-8"));
        String topic = msg.getTopicName();

        logger.info("Message received. Topic: " + topic + ", payload: " + decodedPayload);

        //Try to get DeviceId from routes map by topic
        Integer deviceId = this.topicRoutes.get(topic);
        logger.info("Device Id retrieved: " + deviceId);

        //Try to get device from DeviceRegistry
        Device device = null;
        if (deviceId != null) {
            device = this.registry.getDeviceById(deviceId);
            logger.info("Device retrieved from registry: " + device);
        }

        if (device != null && !decodedPayload.trim().isEmpty()) {
            this.resolver.resolve(device, decodedPayload);
        }
    }

    public void addRoute(String topic, Integer deviceId) {
        this.topicRoutes.put(topic, deviceId);
    }
}

package lv.artjomsfedosenko.sonoffcontrolltest.mqtt;

import io.moquette.interception.AbstractInterceptHandler;
import io.moquette.interception.messages.*;
import lv.artjomsfedosenko.sonoffcontrolltest.domain.Module;
import lv.artjomsfedosenko.sonoffcontrolltest.domain.ModuleStatus;
import lv.artjomsfedosenko.sonoffcontrolltest.events.ModuleStatusChangeEvent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class ModuleWatcher extends AbstractInterceptHandler {

    private static Logger logger = LogManager.getLogger(ModuleWatcher.class);

    private ApplicationEventPublisher eventPublisher;

    private Map<String, Module> modules = new HashMap<>();

    @Autowired
    public ModuleWatcher(ApplicationEventPublisher eventPublisher) {
        this.eventPublisher = eventPublisher;
    }

    public Map<String, Module> getModules() {
        return this.modules;
    }

    @Override
    public String getID() {
        return "ModuleWatcher";
    }

    @Override
    public void onConnect(InterceptConnectMessage msg) {
        String clientId = msg.getClientID();

        logger.info("Connected: " + clientId);

        //Check for simultaneous connecting ?
        if (!this.modules.containsKey(clientId)) {

            System.out.println("Adding device");

            Module module = new Module(clientId, ModuleStatus.CONNECTED);
            this.modules.put(clientId, module);

            System.out.println("Module: " + module + " added");
        } else {
            Module module = this.modules.get(clientId);
            module.setModuleStatus(ModuleStatus.CONNECTED);
        }

        this.eventPublisher.publishEvent(new ModuleStatusChangeEvent(
                this, clientId, ModuleStatus.CONNECTED
        ));
    }

    @Override
    public void onDisconnect(InterceptDisconnectMessage msg) {
        String clientId = msg.getClientID();

        logger.info("Module with id: " + clientId + "disconnected");

        Module disconnectedModule = this.modules.get(clientId);
        disconnectedModule.setModuleStatus(ModuleStatus.DISCONNECTED);

        this.eventPublisher.publishEvent(new ModuleStatusChangeEvent(
                this, clientId, ModuleStatus.DISCONNECTED
        ));
    }

    @Override
    public void onSubscribe(InterceptSubscribeMessage msg) {
        String clientId = msg.getClientID();
        String topicName = msg.getTopicFilter();

        logger.info("Module " + clientId + " subscribed to: " + topicName);

        Module module = this.modules.get(clientId);
        if (module != null) {
            module.addSubscribedTopic(topicName);
        }
    }

    @Override
    public void onUnsubscribe(InterceptUnsubscribeMessage msg) {
        String clientId = msg.getClientID();
        String topicName = msg.getTopicFilter();

        logger.info("Module " + clientId + " unsubscribed from: " + topicName);

        Module module = this.modules.get(clientId);
        if (module != null) {
            module.removeSubscribedTopic(topicName);
        }
    }

    @Override
    public void onConnectionLost(InterceptConnectionLostMessage msg) {
        String clientId = msg.getClientID();

        logger.info("Module with id: " + clientId + "lost connection");

        Module disconnectedModule = this.modules.get(clientId);
        disconnectedModule.setModuleStatus(ModuleStatus.CONNECTION_LOST);

        this.eventPublisher.publishEvent(new ModuleStatusChangeEvent(
                this, clientId, ModuleStatus.CONNECTION_LOST
        ));
    }
}

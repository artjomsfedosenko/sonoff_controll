package lv.artjomsfedosenko.sonoffcontrolltest.mqtt;

import io.moquette.interception.AbstractInterceptHandler;
import io.moquette.interception.messages.InterceptPublishMessage;
import io.netty.buffer.ByteBufUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import java.nio.charset.Charset;

@Component
public class PublishLogListener extends AbstractInterceptHandler {

    private static Logger logger = LogManager.getLogger(PublishLogListener.class);

    @Override
    public String getID() {
        return "PublishLogListener";
    }

    @Override
    public void onPublish(InterceptPublishMessage msg) {

        final String decodedPayload = new String(ByteBufUtil.getBytes(msg.getPayload()), Charset.forName("UTF-8"));
        logger.info("RECEIVED ON TOPIC: " + msg.getTopicName() + " content: " + decodedPayload);
    }
}

package lv.artjomsfedosenko.sonoffcontrolltest.services;

import lv.artjomsfedosenko.sonoffcontrolltest.commands.CommandHandlerInterface;
import lv.artjomsfedosenko.sonoffcontrolltest.commands.CommandInterface;
import lv.artjomsfedosenko.sonoffcontrolltest.commands.CommandResultInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class CommandExecutor {

    private Map<Class, CommandHandlerInterface> handlers = new HashMap<>();

    @Autowired
    public CommandExecutor(List<CommandHandlerInterface> handlers) {

        handlers.forEach(h -> {
            this.handlers.put(h.getCommandType(), h);
        });
    }

    public <R extends CommandResultInterface> R execute(CommandInterface command) {

        CommandHandlerInterface handler = handlers.get(command.getClass());
        if(handler != null) {
            return (R) handler.execute(command);
        } else {
            throw new IllegalArgumentException("Unknown command! " + command.toString());
        }
    }
}

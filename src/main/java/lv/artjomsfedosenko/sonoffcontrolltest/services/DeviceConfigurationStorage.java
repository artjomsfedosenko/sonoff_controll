package lv.artjomsfedosenko.sonoffcontrolltest.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import lv.artjomsfedosenko.sonoffcontrolltest.domain.DeviceConfiguration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

//TODO: implement custom exceptions for storage
@Component
public class DeviceConfigurationStorage {

    /*
    * Persists Device configurations, Serializes and deserializes them
    * and provides access to device configurations
    * */
    private static Logger logger = LogManager.getLogger(DeviceConfigurationStorage.class);

    private String storageFolder;

    private ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    public DeviceConfigurationStorage(@Value("${device-storage.folder}") String folder) {
        this.storageFolder = folder;
        new File(this.storageFolder).mkdirs();
    }

    //Should throw an exception
    public Integer save(DeviceConfiguration config) throws Exception {

        logger.info("Started saving config sequence");
        //Generate Id
        Integer deviceId = this.generateDeviceId();
        config.setId(deviceId);
        logger.info("Genereated new Device Id: " + deviceId);

        //Convert to Json
        String jsonResult = this.objectMapper.writeValueAsString(config);
        logger.info("Converted config to JSON");

        //Check for existence
        File file = new File(this.storageFolder + "/" + deviceId + ".json");
        logger.info("Checked existnece of: " + this.storageFolder + "/" + deviceId + ".json");

        //Save to file
        try (FileWriter fileWriter = new FileWriter(file)) {
            logger.info("Saving config to file");
            fileWriter.write(jsonResult);
        } catch (Exception e) {
            logger.warn(e.getMessage());
            throw e;
        }

        return deviceId;
    }

    public List<DeviceConfiguration> getDeviceConfigurations() throws Exception {

        List<DeviceConfiguration> configurations = new ArrayList<>();

        File folder = new File(this.storageFolder);
        File[] files = folder.listFiles();

        if (files != null) {
            for (File file : files) {

                //Read file contents
                String configJson = this.readFileContents(file);

                //Instantiate DeviceConfig
                DeviceConfiguration configuration = this.objectMapper
                        .readValue(configJson, DeviceConfiguration.class);

                //Add to list
                configurations.add(configuration);
            }
        }

        return configurations;
    }

    private Integer generateDeviceId() {

        File directory = new File(this.storageFolder);
        File[] files = directory.listFiles();

        Integer deviceId = 1;
        if (files != null && files.length > 0) {
            deviceId = files.length + 1;
        }

        return deviceId;
    }

    private String readFileContents(File file) throws Exception {

        String configJson = "";
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {

            String tmp;
            while ((tmp = reader.readLine()) != null) {
                configJson += tmp;
            }

        } catch (Exception e) {
            throw e;
        }

        return configJson;
    }
}

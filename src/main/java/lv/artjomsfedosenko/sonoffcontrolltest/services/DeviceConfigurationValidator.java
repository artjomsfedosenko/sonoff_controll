package lv.artjomsfedosenko.sonoffcontrolltest.services;


import lv.artjomsfedosenko.sonoffcontrolltest.domain.DeviceConfiguration;
import lv.artjomsfedosenko.sonoffcontrolltest.domain.Module;
import lv.artjomsfedosenko.sonoffcontrolltest.mqtt.ModuleWatcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.google.common.base.Preconditions.*;

@Component
public class DeviceConfigurationValidator {

    private DeviceConfigurationStorage storage;

    private ModuleWatcher moduleWatcher;

    //private List<String> topics;

    @Autowired
    public DeviceConfigurationValidator(ModuleWatcher moduleWatcher, DeviceConfigurationStorage storage) {
        this.storage = storage;
        this.moduleWatcher = moduleWatcher;
    }

    public void validate(DeviceConfiguration configuration) {

        validateStringValue(configuration.getName(), "Device name");
        validateStringValue(configuration.getActionTopic(), "Action topic");
        validateStringValue(configuration.getStateTopic(), "State topic");
        //validateStringValue(configuration.getStatusTopic(), "Status topic");
        validateStringValue(configuration.getTelemetryTopic(), "Telemetry topic");
        validateStringValue(configuration.getModuleId(), "Module Id");

        validateTopicIsNotUsed(configuration.getActionTopic());
        validateTopicIsNotUsed(configuration.getTelemetryTopic());

        validateModuleExists(configuration.getModuleId());
    }

    private void validateStringValue(String parameter, String paramName) {
        checkNotNull(parameter, paramName + " should not be NULL");
        checkArgument(!StringUtils.isEmpty(parameter), paramName + " should not be empty");
    }

    //TODO: check for device Id for application "cold strat" case
    private void validateTopicIsNotUsed(String topic) {

        List<DeviceConfiguration> configurations = null;
        try {
            configurations = storage.getDeviceConfigurations();
        } catch (Exception e) {
            //TODO: write to log
            throw new RuntimeException("");
        }

        for (DeviceConfiguration configuration : configurations) {

            checkArgument(
                    !topic.equals(configuration.getActionTopic()),
                    "Topic already in use by " + configuration.getName())
            ;
            checkArgument(
                    !topic.equals(configuration.getStateTopic()),
                    "Topic already in use by " + configuration.getName()
            );
        }
    }

    private void validateModuleExists(String moduleId) {

        Set<String> moduleIds = this.moduleWatcher.getModules().keySet();
        checkArgument(moduleIds.contains(moduleId), "Module Id " + moduleId + " is invalid");
    }
}

package lv.artjomsfedosenko.sonoffcontrolltest.services;

import lv.artjomsfedosenko.sonoffcontrolltest.domain.DeviceConfiguration;
import lv.artjomsfedosenko.sonoffcontrolltest.domain.DeviceTypes;
import lv.artjomsfedosenko.sonoffcontrolltest.domain.devices.*;
import org.springframework.stereotype.Component;

@Component
public class DeviceFactory {

    public Device createDevice(DeviceConfiguration configuration) {

        DeviceTypes deviceType = configuration.getDeviceType();

        Device device = null;
        switch (deviceType) {
            case SWITCH:
                device = this.createSwitch(configuration);
                break;
            case TEMPERATURE_SENSOR:
                device = this.createTemperatureSensor(configuration);
                break;
            case TEMPERTATURE_HUMIDITY_SENSOR:
                device = this.createTemperatureHumiditySensor(configuration);
                break;
            default:
                throw new RuntimeException("Unsupported device type");
        }

        return device;
    }

    private Switch createSwitch(DeviceConfiguration configuration) {

        Switch device = new Switch();

        this.setCommonParameters(device, configuration);
        device.setSwitchPosition(SwitchPosition.OFF)
                .setSwitchState(SwitchState.OFF);

        return device;
    }

    private TemperatureSensor createTemperatureSensor(DeviceConfiguration configuration) {

        TemperatureSensor sensor = new TemperatureSensor();
        this.setCommonParameters(sensor, configuration);
        sensor.setTemperature(null);

        return sensor;
    }

    private TemperatureHumiditySensor createTemperatureHumiditySensor(DeviceConfiguration configuration) {

        TemperatureHumiditySensor sensor = new TemperatureHumiditySensor();
        this.setCommonParameters(sensor, configuration);
        sensor.setTemperature(null)
                .setHumidity(null);

        return sensor;
    }

    private void setCommonParameters(Device device, DeviceConfiguration configuration) {

        device.setId(configuration.getId())
                .setName(configuration.getName())
                .setActionTopic(configuration.getActionTopic())
                //.setStatusTopic(configuration.getStatusTopic())
                .setStateTopic(configuration.getStateTopic())
                .setTelemetryTopic(configuration.getTelemetryTopic())
                .setModuleId(configuration.getModuleId())
                .setDeviceStatus(DeviceStatus.OFFLINE);
    }
}

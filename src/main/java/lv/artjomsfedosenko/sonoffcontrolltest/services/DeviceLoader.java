package lv.artjomsfedosenko.sonoffcontrolltest.services;

import lv.artjomsfedosenko.sonoffcontrolltest.domain.DeviceConfiguration;
import lv.artjomsfedosenko.sonoffcontrolltest.domain.devices.Device;
import lv.artjomsfedosenko.sonoffcontrolltest.events.DeviceAddedEvent;
import lv.artjomsfedosenko.sonoffcontrolltest.mqtt.DeviceStateRouter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

@Component
public class DeviceLoader {

    private static Logger logger = LogManager.getLogger(DeviceLoader.class);

    private ApplicationEventPublisher eventPublisher;

    private DeviceFactory deviceFactory;

    private DeviceRegistry deviceRegistry;

    private DeviceStateRouter router;

    @Autowired
    public DeviceLoader(
            ApplicationEventPublisher eventPublisher,
            DeviceFactory deviceFactory,
            DeviceRegistry deviceRegistry,
            DeviceStateRouter router
    ) {
        this.eventPublisher = eventPublisher;
        this.deviceFactory = deviceFactory;
        this.deviceRegistry = deviceRegistry;
        this.router = router;
    }

    public Device loadDevice(DeviceConfiguration configuration) {

        logger.info("Started loading device sequence");
        //Request device from factory
        Device device = this.deviceFactory.createDevice(configuration);
        logger.info("Received device from factory: " + device);

        //Add to Device registry
        this.deviceRegistry.addDevice(device);
        logger.info("Added device " + device.getId() + " to device registry");

        this.router.addRoute(device.getStateTopic(), device.getId());
        this.router.addRoute(device.getTelemetryTopic(), device.getId());
        logger.info("Added topics: " + device.getActionTopic() + ", " + device.getTelemetryTopic() + " Device: " + device);

        //Publish DeviceAdded event
        DeviceAddedEvent event = new DeviceAddedEvent(this, device.getId());
        this.eventPublisher.publishEvent(event);
        logger.info("Device added event triggered for: " +  device.getId());

        return device;
    }
}

package lv.artjomsfedosenko.sonoffcontrolltest.services;

import lv.artjomsfedosenko.sonoffcontrolltest.domain.devices.Device;
import lv.artjomsfedosenko.sonoffcontrolltest.domain.devices.DeviceStatus;
import lv.artjomsfedosenko.sonoffcontrolltest.mqtt.Broker;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class DeviceRegistry {

    private static Logger logger = LogManager.getLogger(DeviceRegistry.class);

    //DeviceId, Device
    private Map<Integer, Device> devices = new HashMap<>();

    //ModuleId, List of devices
    private Map<String, List<Device>> moduleDeviceMap = new HashMap<>();

    public Map<Integer, Device> getDevices() {
        return this.devices;
    }

    public void addDevice(Device device) {
        logger.info("Adding device: " + device);
        this.devices.put(device.getId(), device);
        logger.info("Device added");

        this.addDeviceToModuleDeviceMap(device);
    }

    public String getDeviceModuleId(Integer deviceId) {
        return this.getDeviceById(deviceId).getModuleId();
    }

    //TODO: extract this functionality to separate class, or to invoker
    public void setDeviceStatus(Integer deviceId, DeviceStatus deviceStatus) {

        Device device = this.getDeviceById(deviceId);
        device.setDeviceStatus(deviceStatus);
    }

    //TODO: extract this functionality to separate class, or to invoker
    public void setDeviceStatusByModuleId(String moduleId, DeviceStatus deviceStatus) {

        //Try to get devices by module id
        List<Device> devices = this.moduleDeviceMap.get(moduleId);

        //If no such devices - inform
        logger.info("Devices defined for module: " + moduleId);

        //Update each device status
        if (devices != null) {
            for (Device device : devices) {
                device.setDeviceStatus(deviceStatus);
            }
        }
    }

    public Device getDeviceById(Integer deviceId) {

        Device device = this.devices.get(deviceId);
        if (device == null) {
            throw new IllegalArgumentException("No device with id: " + deviceId);
        }

        return device;
    }

    private void addDeviceToModuleDeviceMap(Device device) {

        //Try to get the list from map
        logger.info("Adding device: " + device + " to module device map. Moduel Id: " + device.getModuleId());
        String moduleId = device.getModuleId();
        List<Device> devices = this.moduleDeviceMap.get(moduleId);
        logger.info("Devices for this module: " + devices);

        //If no - create list, add device
        if (devices == null) {
            devices = new ArrayList<>();
            this.moduleDeviceMap.put(moduleId, devices);
            logger.info("Created empty device list for module: " + moduleId);
        }

        devices.add(device);
        logger.info("Added device: " + device + " to module device map");
    }
}

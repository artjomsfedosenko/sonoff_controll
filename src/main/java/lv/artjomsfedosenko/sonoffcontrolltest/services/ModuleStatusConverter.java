package lv.artjomsfedosenko.sonoffcontrolltest.services;

import lv.artjomsfedosenko.sonoffcontrolltest.domain.ModuleStatus;
import lv.artjomsfedosenko.sonoffcontrolltest.domain.devices.DeviceStatus;
import org.springframework.stereotype.Component;

@Component
public class ModuleStatusConverter {

    public DeviceStatus getDeviceStatusByModuleStatus(ModuleStatus moduleStatus) {

        DeviceStatus deviceStatus = null;

        switch (moduleStatus) {
            case CONNECTED:
                deviceStatus = DeviceStatus.ONLINE;
                break;
            case DISCONNECTED:
                deviceStatus = DeviceStatus.OFFLINE;
                break;
            case CONNECTION_LOST:
                deviceStatus = DeviceStatus.OFFLINE;
                break;
            default:
                throw new IllegalArgumentException("Unsupported Module status");
        }

        return deviceStatus;
    }

}

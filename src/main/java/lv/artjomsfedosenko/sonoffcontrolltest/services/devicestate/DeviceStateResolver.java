package lv.artjomsfedosenko.sonoffcontrolltest.services.devicestate;

import lv.artjomsfedosenko.sonoffcontrolltest.domain.devices.Device;
import lv.artjomsfedosenko.sonoffcontrolltest.domain.devices.Switch;
import lv.artjomsfedosenko.sonoffcontrolltest.domain.devices.TemperatureHumiditySensor;
import lv.artjomsfedosenko.sonoffcontrolltest.domain.devices.TemperatureSensor;
import lv.artjomsfedosenko.sonoffcontrolltest.services.devicestate.strategy.DeviceStateResolutionStrategy;
import lv.artjomsfedosenko.sonoffcontrolltest.services.devicestate.strategy.SwitchStrategy;
import lv.artjomsfedosenko.sonoffcontrolltest.services.devicestate.strategy.TemperatureHumiditySensorStrategy;
import lv.artjomsfedosenko.sonoffcontrolltest.services.devicestate.strategy.TemperatureSensorStrategy;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

@Component
public class DeviceStateResolver {

    private static Logger logger = LogManager.getLogger(DeviceStateResolver.class);

    public void resolve(Device device, String messagePayload) {

        logger.info("Resolving device: " + device + " state");
        DeviceStateResolutionStrategy strategy = this.chooseStrategy(device);
        strategy.setDeviceState(device, messagePayload);
    }

    private DeviceStateResolutionStrategy chooseStrategy(Device device) {

        DeviceStateResolutionStrategy strategy = null;
        if (device instanceof Switch) {
            strategy = new SwitchStrategy();
        }

        if (device instanceof TemperatureSensor) {
            strategy = new TemperatureSensorStrategy();
        }

        if (device instanceof TemperatureHumiditySensor) {
            strategy = new TemperatureHumiditySensorStrategy();
        }

        return strategy;
    }
}

package lv.artjomsfedosenko.sonoffcontrolltest.services.devicestate.strategy;

import lv.artjomsfedosenko.sonoffcontrolltest.domain.devices.Device;

public interface DeviceStateResolutionStrategy {

    void setDeviceState(Device device, String messagePayload);

}

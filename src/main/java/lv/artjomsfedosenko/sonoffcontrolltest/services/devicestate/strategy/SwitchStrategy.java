package lv.artjomsfedosenko.sonoffcontrolltest.services.devicestate.strategy;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lv.artjomsfedosenko.sonoffcontrolltest.domain.devices.Device;
import lv.artjomsfedosenko.sonoffcontrolltest.domain.devices.Switch;
import lv.artjomsfedosenko.sonoffcontrolltest.domain.devices.SwitchPosition;
import lv.artjomsfedosenko.sonoffcontrolltest.domain.devices.SwitchState;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

public class SwitchStrategy implements DeviceStateResolutionStrategy {

    private static Logger logger = LogManager.getLogger(SwitchStrategy.class);

    private final Map<SwitchState, SwitchPosition> positionMapping = new HashMap<>();

    public SwitchStrategy() {
        this.positionMapping.put(SwitchState.OFF, SwitchPosition.OFF);
        this.positionMapping.put(SwitchState.ON, SwitchPosition.ON);
    }

    @Override
    public void setDeviceState(Device device, String messagePayload) {

        Switch switchDevice = (Switch) device;

        String payloadValue = null;
        if (this.isTelemetryPayload(messagePayload)) {
            payloadValue = this.parseTelemetryPayload(messagePayload);
        } else {
            payloadValue = messagePayload;
        }

        logger.info("Decoded payload: " + payloadValue);
        //Try defining state
        SwitchState state = SwitchState.valueOf(payloadValue);

        //Set state
        switchDevice.setSwitchState(state);
        this.setSwitchPositionBySwitchState(state, switchDevice);
    }

    private Boolean isTelemetryPayload(String messagePayload) {

        if (messagePayload.startsWith("{")) {
            return Boolean.valueOf(true);
        }

        return Boolean.valueOf(false);
    }

    private String parseTelemetryPayload(String payload) {

        ObjectMapper mapper = new ObjectMapper();

        JsonNode parsedJson = null;
        try {
            parsedJson = mapper.readTree(payload);
        } catch (Exception e) {
            logger.warn("Failed parsing payload: " + payload);
        }

        String stateValue = parsedJson.get("POWER").textValue();

        return stateValue;
    }

    private void setSwitchPositionBySwitchState(SwitchState switchState, Switch device) {

        SwitchPosition currentPosition = device.getSwitchPosition();
        SwitchPosition provisionalPosition = this.positionMapping.get(switchState);

        if (currentPosition != provisionalPosition) {
            device.setSwitchPosition(provisionalPosition);
        }
    }
}

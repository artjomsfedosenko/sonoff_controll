package lv.artjomsfedosenko.sonoffcontrolltest.services.devicestate.strategy;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lv.artjomsfedosenko.sonoffcontrolltest.domain.devices.Device;
import lv.artjomsfedosenko.sonoffcontrolltest.domain.devices.TemperatureHumiditySensor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TemperatureHumiditySensorStrategy implements DeviceStateResolutionStrategy {

    private static Logger logger = LogManager.getLogger(TemperatureHumiditySensorStrategy.class);

    @Override
    public void setDeviceState(Device device, String messagePayload) {

        TemperatureHumiditySensor sensor = (TemperatureHumiditySensor) device;

        JsonNode parsedJson = this.parsePayload(messagePayload);
        if (!this.isTelemetryPayload(parsedJson)) {
            parsedJson = parsedJson.get("StatusSNS");
        }

        Float temperature = this.parseTemperature(parsedJson);
        Float humidity = this.parseHumidity(parsedJson);

        sensor.setTemperature(temperature);
        sensor.setHumidity(humidity);
    }

    private Boolean isTelemetryPayload(JsonNode json) {

        if (!json.has("StatusSNS")) {
            return Boolean.valueOf(true);
        }

        return Boolean.valueOf(false);
    }

    private JsonNode parsePayload(String messagePayload) {

        ObjectMapper mapper = new ObjectMapper();

        JsonNode parsedJson = null;
        try {
            parsedJson = mapper.readTree(messagePayload);
        } catch (Exception e) {
            logger.warn("Failed parsing payload: " + messagePayload);
        }

        return parsedJson;
    }

    private Float parseTemperature(JsonNode json) {

        String temperatureValue = null;
        try {
            temperatureValue = json.get("AM2301").get("Temperature").asText();
        } catch (Exception e) {
            logger.warn("Failed parsing json: " + json);
        }

        return Float.valueOf(temperatureValue);
    }

    private Float parseHumidity(JsonNode json) {
        String humidityValue = null;
        try {
            humidityValue = json.get("AM2301").get("Humidity").asText();
        } catch (Exception e) {
            logger.warn("Failed parsing json: " + json);
        }

        return Float.valueOf(humidityValue);
    }
}

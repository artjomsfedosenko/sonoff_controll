package lv.artjomsfedosenko.sonoffcontrolltest.services.devicestate.strategy;

import lv.artjomsfedosenko.sonoffcontrolltest.domain.devices.Device;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TemperatureSensorStrategy implements DeviceStateResolutionStrategy {

    private static Logger logger = LogManager.getLogger(TemperatureSensorStrategy.class);

    @Override
    public void setDeviceState(Device device, String messagePayload) {
        logger.warn("Sensor is unsupported yet");
        throw new IllegalArgumentException("Sensor is unsupported yet");
    }
}

package lv.artjomsfedosenko.sonoffcontrolltest.commands;

import lv.artjomsfedosenko.sonoffcontrolltest.domain.DeviceConfiguration;
import lv.artjomsfedosenko.sonoffcontrolltest.domain.devices.Device;
import lv.artjomsfedosenko.sonoffcontrolltest.services.*;
import org.junit.Before;
import org.junit.Test;

import javax.security.auth.login.Configuration;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class AddDeviceCommandHandlerTest {

    private AddDeviceCommandHandler commandHandler;

    private DeviceConfigurationStorage deviceConfigurationStorage;

    private DeviceConfigurationValidator deviceConfigurationValidator;

    private DeviceFactory deviceFactory;

    private DeviceRegistry deviceRegistry;

    private DeviceLoader deviceLoader;

    @Before
    public void setUp() throws Exception {

        DeviceConfigurationStorage storage = mock(DeviceConfigurationStorage.class);
        when(storage.save(any(DeviceConfiguration.class))).thenReturn(123);

        DeviceConfigurationValidator deviceConfigurationValidator = mock(DeviceConfigurationValidator.class);

        Device device = new Device().setId(123);
        DeviceLoader deviceLoader = mock(DeviceLoader.class);
        when(deviceLoader.loadDevice(any(DeviceConfiguration.class))).thenReturn(device);

        AddDeviceCommandHandler commandHandler = new AddDeviceCommandHandler(
                storage, deviceConfigurationValidator, deviceLoader
        );

        this.commandHandler = commandHandler;
        this.deviceConfigurationStorage = storage;
        this.deviceConfigurationValidator = deviceConfigurationValidator;
        this.deviceLoader = deviceLoader;
    }

    @Test
    //TODO: modify test, when Exception hierarchy will be implemented
    public void testExecuteCallsExpectedMethods() throws Exception {

        AddDeviceCommand command = new AddDeviceCommand().setDeviceType("SWITCH");
        AddDeviceResult result = this.commandHandler.execute(command);

        verify(this.deviceConfigurationValidator).validate(any(DeviceConfiguration.class));
        verify(this.deviceConfigurationStorage).save(any(DeviceConfiguration.class));
        verify(this.deviceLoader).loadDevice(any(DeviceConfiguration.class));

        assertEquals(Integer.valueOf(123), result.getDeviceId());
    }

    @Test
    public void testGetCommandTypeShouldReturnExpectedCommandType() {
        assertEquals(AddDeviceCommand.class, this.commandHandler.getCommandType());
    }
}
package lv.artjomsfedosenko.sonoffcontrolltest.commands;

import lv.artjomsfedosenko.sonoffcontrolltest.domain.devices.*;
import lv.artjomsfedosenko.sonoffcontrolltest.dto.DeviceDTO;
import lv.artjomsfedosenko.sonoffcontrolltest.dto.SwitchDTO;
import lv.artjomsfedosenko.sonoffcontrolltest.dto.TemperatureHumiditySensorDTO;
import lv.artjomsfedosenko.sonoffcontrolltest.dto.TemperatureSensorDTO;
import lv.artjomsfedosenko.sonoffcontrolltest.services.DeviceRegistry;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class ListDeviceCommandHandlerTest {

    private DeviceRegistry deviceRegistry;

    private ListDeviceCommandHandler handler;


    @Before
    public void setUp() throws Exception {

        Map<Integer, Device> devices = this.prepareTestDevices();
        DeviceRegistry registry = mock(DeviceRegistry.class);
        when(registry.getDevices()).thenReturn(devices);

        ListDeviceCommandHandler handler = new ListDeviceCommandHandler(registry);

        this.deviceRegistry = registry;
        this.handler = handler;
    }

    @Test
    public void testExecuteGetsDevicesFromRegistryAndReturnsExpectedResult() {

        ListDevicesCommand command = new ListDevicesCommand();
        ListDevicesResult result = this.handler.execute(command);

        verify(this.deviceRegistry).getDevices();

        List<DeviceDTO> devices = result.getDevices();

        assertEquals(SwitchDTO.class, devices.get(0).getClass());
        assertEquals(TemperatureSensorDTO.class, devices.get(1).getClass());
        assertEquals(TemperatureHumiditySensorDTO.class, devices.get(2).getClass());

        SwitchDTO switchDTO = (SwitchDTO) devices.get(0);
        assertEquals(Integer.valueOf(1), switchDTO.getId());
        assertEquals("switch", switchDTO.getName());
        assertEquals("ON", switchDTO.getSwitchState());
        assertEquals("ON", switchDTO.getSwitchPosition());

        TemperatureSensorDTO sensorDTO = (TemperatureSensorDTO) devices.get(1);
        assertEquals(Integer.valueOf(2), sensorDTO.getId());
        assertEquals("sensor", sensorDTO.getName());
        assertEquals(Float.valueOf(21.5F), sensorDTO.getTemperature());

        TemperatureHumiditySensorDTO humiditySensorDTO = (TemperatureHumiditySensorDTO) devices.get(2);
        assertEquals(Integer.valueOf(3), humiditySensorDTO.getId());
        assertEquals("humiditySensor", humiditySensorDTO.getName());
        assertEquals(Float.valueOf(23.5F), humiditySensorDTO.getTemperature());
        assertEquals(Float.valueOf(65.5F), humiditySensorDTO.getHumidity());
    }

    @Test
    public void getCommandTypeReturnsExpectedCommand() {
        assertEquals(ListDevicesCommand.class, this.handler.getCommandType());
    }

    private Map<Integer, Device> prepareTestDevices() {

        Map<Integer, Device> deviceMap = new HashMap<>();

        Switch switchDevice = new Switch();
        switchDevice.setId(1)
            .setName("switch")
            .setActionTopic("switchActionTopic")
            .setStateTopic("switchStateTopic")
            .setTelemetryTopic("switchTelemetryTopic")
            .setModuleId("switchModuleId")
            .setDeviceStatus(DeviceStatus.ONLINE);
        switchDevice.setSwitchState(SwitchState.ON)
            .setSwitchPosition(SwitchPosition.ON);

        deviceMap.put(switchDevice.getId(), switchDevice);

        TemperatureSensor sensorDevice = new TemperatureSensor();
        sensorDevice.setId(2)
                .setName("sensor")
                .setActionTopic("sensorActionTopic")
                .setStateTopic("sensorStateTopic")
                .setTelemetryTopic("sensorTelemetryTopic")
                .setModuleId("sensorModuleId")
                .setDeviceStatus(DeviceStatus.ONLINE);
        sensorDevice.setTemperature(21.5F);

        deviceMap.put(sensorDevice.getId(), sensorDevice);

        TemperatureHumiditySensor humidityDevice = new TemperatureHumiditySensor();
        humidityDevice.setId(3)
                .setName("humiditySensor")
                .setActionTopic("humidityActionTopic")
                .setStateTopic("humidityStateTopic")
                .setTelemetryTopic("humidityTelemetryTopic")
                .setModuleId("humidityModuleId")
                .setDeviceStatus(DeviceStatus.ONLINE);
        humidityDevice.setTemperature(23.5F)
                .setHumidity(65.5F);

        deviceMap.put(humidityDevice.getId(), humidityDevice);

        return deviceMap;
    }
}
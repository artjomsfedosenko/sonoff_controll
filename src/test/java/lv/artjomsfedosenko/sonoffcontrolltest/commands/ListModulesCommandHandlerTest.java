package lv.artjomsfedosenko.sonoffcontrolltest.commands;

import lv.artjomsfedosenko.sonoffcontrolltest.domain.Module;
import lv.artjomsfedosenko.sonoffcontrolltest.domain.ModuleStatus;
import lv.artjomsfedosenko.sonoffcontrolltest.dto.ModuleDTO;
import lv.artjomsfedosenko.sonoffcontrolltest.mqtt.ModuleWatcher;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class ListModulesCommandHandlerTest {

    private ListModulesCommandHandler listModulesCommandHandler;

    @Before
    public void setUp() throws Exception {

        Map<String, Module> modules = new HashMap<>();
        modules.put("testModule", new Module("testModule", ModuleStatus.CONNECTED));

        ModuleWatcher moduleWatcher = mock(ModuleWatcher.class);
        when(moduleWatcher.getModules()).thenReturn(modules);

        ListModulesCommandHandler listModulesCommandHandler = new ListModulesCommandHandler(moduleWatcher);
        this.listModulesCommandHandler = listModulesCommandHandler;
    }

    @Test
    public void testExecuteReturnsResultOfCorrectType() throws Exception {

        ListModulesResult result =
                this.listModulesCommandHandler.execute(new ListModulesCommand());

        assertNotNull(result);
        assertEquals(ListModulesResult.class, result.getClass());
    }

    @Test
    public void testExecuteResultContainsModule() throws Exception {

        ListModulesResult result =
                this.listModulesCommandHandler.execute(new ListModulesCommand());

        ModuleDTO module = result.getModules().get(0);

        assertNotNull(module);
        assertEquals("testModule", module.getModuleId());
        assertEquals("CONNECTED", module.getModuleState());
    }

    @Test
    public void testGetCommandTypeReturnsCorrectCommand() {

        assertEquals(ListModulesCommand.class, this.listModulesCommandHandler.getCommandType());
    }
}
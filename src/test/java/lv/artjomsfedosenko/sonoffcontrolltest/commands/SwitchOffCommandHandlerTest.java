package lv.artjomsfedosenko.sonoffcontrolltest.commands;

import lv.artjomsfedosenko.sonoffcontrolltest.domain.devices.Device;
import lv.artjomsfedosenko.sonoffcontrolltest.mqtt.Broker;
import lv.artjomsfedosenko.sonoffcontrolltest.services.DeviceRegistry;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

public class SwitchOffCommandHandlerTest {

    private SwitchOffCommandHandler handler;

    private DeviceRegistry registry;

    private Broker broker;

    @Before
    public void setUp() throws Exception {

        Broker broker = mock(Broker.class);
        DeviceRegistry registry = mock(DeviceRegistry.class);
        Device device = new Device().setId(1).setActionTopic("action/topic");
        when(registry.getDeviceById(1)).thenReturn(device);

        SwitchOffCommandHandler handler = new SwitchOffCommandHandler(
                registry, broker
        );

        this.registry = registry;
        this.broker = broker;
        this.handler = handler;
    }

    @Test
    public void executeSendsExpectedMessageToDeviceActionTopic() {

        SwitchOffCommand command = new SwitchOffCommand(1);
        SwitchOffResult result = this.handler.execute(command);

        verify(this.registry).getDeviceById(1);
        verify(this.broker).publish("action/topic", "OFF");

        assertNotNull(result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void executeOnNonExistingDeviceThrowsException() {
        SwitchOffCommand command = new SwitchOffCommand(7);
        SwitchOffResult result = this.handler.execute(command);
    }

    @Test
    public void getCommandType() {
        assertEquals(SwitchOffCommand.class, this.handler.getCommandType());
    }
}
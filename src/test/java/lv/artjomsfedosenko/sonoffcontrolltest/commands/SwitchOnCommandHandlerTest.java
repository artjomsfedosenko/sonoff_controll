package lv.artjomsfedosenko.sonoffcontrolltest.commands;

import lv.artjomsfedosenko.sonoffcontrolltest.domain.devices.Device;
import lv.artjomsfedosenko.sonoffcontrolltest.mqtt.Broker;
import lv.artjomsfedosenko.sonoffcontrolltest.services.DeviceRegistry;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class SwitchOnCommandHandlerTest {

    private SwitchOnCommandHandler handler;

    private DeviceRegistry registry;

    private Broker broker;

    @Before
    public void setUp() throws Exception {

        Broker broker = mock(Broker.class);
        DeviceRegistry registry = mock(DeviceRegistry.class);
        Device device = new Device().setId(1).setActionTopic("action/topic");
        when(registry.getDeviceById(1)).thenReturn(device);

        SwitchOnCommandHandler handler = new SwitchOnCommandHandler(
                registry, broker
        );

        this.registry = registry;
        this.broker = broker;
        this.handler = handler;
    }

    @Test
    public void executeSendsExpectedMessageToDeviceActionTopic() {

        SwitchOnCommand command = new SwitchOnCommand(1);
        SwitchOnResult result = this.handler.execute(command);

        verify(this.registry).getDeviceById(1);
        verify(this.broker).publish("action/topic", "ON");

        assertNotNull(result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void executeOnNonExistingDeviceThrowsException() {
        SwitchOnCommand command = new SwitchOnCommand(7);
        SwitchOnResult result = this.handler.execute(command);
    }

    @Test
    public void getCommandType() {
        assertEquals(SwitchOnCommand.class, this.handler.getCommandType());
    }
}
package lv.artjomsfedosenko.sonoffcontrolltest.eventlistener;

import lv.artjomsfedosenko.sonoffcontrolltest.domain.DeviceConfiguration;
import lv.artjomsfedosenko.sonoffcontrolltest.domain.DeviceTypes;
import lv.artjomsfedosenko.sonoffcontrolltest.domain.devices.Device;
import lv.artjomsfedosenko.sonoffcontrolltest.services.DeviceConfigurationStorage;
import lv.artjomsfedosenko.sonoffcontrolltest.services.DeviceLoader;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class ApplicationInitializationListenerTest {

    private DeviceConfigurationStorage configurationStorage;

    private DeviceLoader deviceLoader;

    private List<DeviceConfiguration> configurations;

    private ApplicationInitializationListener listener;

    private List<Device> devices;

    @Before
    public void setUp() throws Exception {

        this.configurations = this.prepareDeviceConfigurations();
        DeviceConfigurationStorage configurationStorage = mock(DeviceConfigurationStorage.class);
        when(configurationStorage.getDeviceConfigurations()).thenReturn(this.configurations);

        this.devices = this.prepareTestDevices();
        DeviceLoader deviceLoader = mock(DeviceLoader.class);
        when(deviceLoader.loadDevice(this.configurations.get(0))).thenReturn(this.devices.get(0));
        when(deviceLoader.loadDevice(this.configurations.get(1))).thenReturn(this.devices.get(1));

        ApplicationInitializationListener listener = new ApplicationInitializationListener(
                configurationStorage, deviceLoader
        );

        this.configurationStorage = configurationStorage;
        this.deviceLoader = deviceLoader;
        this.listener = listener;
    }

    @Test
    public void onApplicationEventLoadsDevicesIntoMemory() throws Exception {

        ApplicationContext context = new AnnotationConfigApplicationContext();
        ContextRefreshedEvent event = new ContextRefreshedEvent(context);
        this.listener.onApplicationEvent(event);

        verify(this.configurationStorage).getDeviceConfigurations();
        verify(this.deviceLoader).loadDevice(this.configurations.get(0));
        verify(this.deviceLoader).loadDevice(this.configurations.get(1));
    }

    private List<DeviceConfiguration> prepareDeviceConfigurations() {

        List<DeviceConfiguration> configurations = new ArrayList<>();

        DeviceConfiguration configuration1 = new DeviceConfiguration()
                .setId(1)
                .setName("device1")
                .setActionTopic("actionTopic1")
                .setStateTopic("stateTopic1")
                .setTelemetryTopic("teleTopic1")
                .setDeviceType(DeviceTypes.SWITCH)
                .setModuleId("moduleId1");

        DeviceConfiguration configuration2 = new DeviceConfiguration()
                .setId(2)
                .setName("device2")
                .setActionTopic("actionTopic2")
                .setStateTopic("stateTopic2")
                .setTelemetryTopic("teleTopic2")
                .setDeviceType(DeviceTypes.TEMPERATURE_SENSOR)
                .setModuleId("moduleId2");

        configurations.add(configuration1);
        configurations.add(configuration2);

        return configurations;
    }

    private List<Device> prepareTestDevices() {

        List<Device> devices = new ArrayList<>();

        Device device1 = new Device()
                .setId(1);

        Device device2 = new Device()
                .setId(2);
        devices.add(device1);
        devices.add(device2);

        return devices;
    }
}
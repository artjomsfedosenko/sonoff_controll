package lv.artjomsfedosenko.sonoffcontrolltest.eventlistener;

import lv.artjomsfedosenko.sonoffcontrolltest.domain.Module;
import lv.artjomsfedosenko.sonoffcontrolltest.domain.ModuleStatus;
import lv.artjomsfedosenko.sonoffcontrolltest.domain.devices.DeviceStatus;
import lv.artjomsfedosenko.sonoffcontrolltest.events.DeviceAddedEvent;
import lv.artjomsfedosenko.sonoffcontrolltest.mqtt.ModuleWatcher;
import lv.artjomsfedosenko.sonoffcontrolltest.services.DeviceRegistry;
import lv.artjomsfedosenko.sonoffcontrolltest.services.ModuleStatusConverter;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.*;

public class DeviceAddedListenerTest {

    private DeviceAddedListener listener;

    private ModuleWatcher moduleWatcher;

    private DeviceRegistry deviceRegistry;

    private ModuleStatusConverter converter;

    @Before
    public void setUp() throws Exception {

        DeviceRegistry deviceRegistry = mock(DeviceRegistry.class);
        when(deviceRegistry.getDeviceModuleId(213)).thenReturn("moduleId");

        ModuleWatcher moduleWatcher = mock(ModuleWatcher.class);

        Map<String, Module> modules = new HashMap<>();
        Module module = new Module("moduleId", ModuleStatus.CONNECTED);
        modules.put(module.getModuleId(), module);
        when(moduleWatcher.getModules()).thenReturn(modules);

        ModuleStatusConverter converter = mock(ModuleStatusConverter.class);
        when(converter.getDeviceStatusByModuleStatus(ModuleStatus.CONNECTED)).thenReturn(DeviceStatus.ONLINE);

        this.moduleWatcher = moduleWatcher;
        this.deviceRegistry = deviceRegistry;
        this.converter = converter;
        this.listener = new DeviceAddedListener(moduleWatcher, deviceRegistry, converter);
    }

    @Test
    public void testOnDeviceAddedEventDeviceStateIsChecked() {

        DeviceAddedEvent event = new DeviceAddedEvent(this, 213);
        this.listener.onApplicationEvent(event);

        verify(this.deviceRegistry).getDeviceModuleId(213);
        verify(this.moduleWatcher).getModules();
        verify(this.converter).getDeviceStatusByModuleStatus(ModuleStatus.CONNECTED);
        verify(this.deviceRegistry).setDeviceStatus(213, DeviceStatus.ONLINE);
    }
}
package lv.artjomsfedosenko.sonoffcontrolltest.eventlistener;

import lv.artjomsfedosenko.sonoffcontrolltest.domain.ModuleStatus;
import lv.artjomsfedosenko.sonoffcontrolltest.domain.devices.DeviceStatus;
import lv.artjomsfedosenko.sonoffcontrolltest.events.ModuleStatusChangeEvent;
import lv.artjomsfedosenko.sonoffcontrolltest.services.DeviceRegistry;
import lv.artjomsfedosenko.sonoffcontrolltest.services.ModuleStatusConverter;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.*;

public class ModuleStatusChangeListenerTest {

    private ModuleStatusConverter converter;

    private DeviceRegistry deviceRegistry;

    private ModuleStatusChangeListener listener;

    @Before
    public void setUp() throws Exception {

        ModuleStatusConverter converter = mock(ModuleStatusConverter.class);
        when(converter.getDeviceStatusByModuleStatus(ModuleStatus.CONNECTED)).thenReturn(DeviceStatus.ONLINE);

        DeviceRegistry deviceRegistry = mock(DeviceRegistry.class);

        ModuleStatusChangeListener listener = new ModuleStatusChangeListener(
                deviceRegistry, converter
        );

        this.converter = converter;
        this.deviceRegistry = deviceRegistry;
        this.listener = listener;
    }

    @Test
    public void onApplicationEventUpdatesDeviceStatus() {

        ModuleStatusChangeEvent event = new ModuleStatusChangeEvent(
                this, "module1", ModuleStatus.CONNECTED
        );

        this.listener.onApplicationEvent(event);

        verify(this.converter).getDeviceStatusByModuleStatus(ModuleStatus.CONNECTED);
        verify(this.deviceRegistry).setDeviceStatusByModuleId("module1", DeviceStatus.ONLINE);


    }
}
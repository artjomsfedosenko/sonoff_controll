package lv.artjomsfedosenko.sonoffcontrolltest.mqtt;

import io.moquette.interception.AbstractInterceptHandler;
import io.moquette.interception.InterceptHandler;
import io.moquette.interception.messages.*;
import io.moquette.server.Server;
import io.moquette.spi.impl.ProtocolProcessor;
import io.netty.buffer.ByteBufUtil;
import io.netty.handler.codec.mqtt.MqttPublishMessage;
import lv.artjomsfedosenko.sonoffcontrolltest.SonoffControllTestApplication;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;

import java.lang.reflect.Field;
import java.nio.charset.Charset;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BrokerTest {

    @Autowired
    ApplicationContext context;

    @Autowired
    private Broker broker;

    @Test
    public void testInterceptHandlersAutowiredCorrectly() throws Exception {

        Field interceptorsField = this.broker.getClass().getDeclaredField("interceptors");
        interceptorsField.setAccessible(true);
        List<? extends AbstractInterceptHandler> interceptors =
                (List<? extends AbstractInterceptHandler>) interceptorsField.get(this.broker);

        assertTrue(interceptors.size() > 0);
    }

    @Test
    public void testPublishAttemptsToPublishMessageUsingMqttServer() {

        this.broker.publish("test", "test");
        Server server = this.context.getBean(Server.class);

        server.addInterceptHandler(new InterceptHandler() {
            @Override
            public String getID() {
                return "testHandler";
            }

            @Override
            public Class<?>[] getInterceptedMessageTypes() {
                return new Class[0];
            }

            @Override
            public void onConnect(InterceptConnectMessage msg) {

            }

            @Override
            public void onDisconnect(InterceptDisconnectMessage msg) {

            }

            @Override
            public void onConnectionLost(InterceptConnectionLostMessage msg) {

            }

            @Override
            public void onPublish(InterceptPublishMessage msg) {
                String testMessage = new String(ByteBufUtil.getBytes(msg.getPayload()), Charset.forName("UTF-8"));
                assertEquals("test", testMessage);
            }

            @Override
            public void onSubscribe(InterceptSubscribeMessage msg) {

            }

            @Override
            public void onUnsubscribe(InterceptUnsubscribeMessage msg) {

            }

            @Override
            public void onMessageAcknowledged(InterceptAcknowledgedMessage msg) {

            }
        });
    }
}
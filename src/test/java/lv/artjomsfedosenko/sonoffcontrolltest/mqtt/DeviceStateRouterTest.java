package lv.artjomsfedosenko.sonoffcontrolltest.mqtt;

import io.moquette.interception.messages.InterceptPublishMessage;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import lv.artjomsfedosenko.sonoffcontrolltest.domain.devices.DeviceStatus;
import lv.artjomsfedosenko.sonoffcontrolltest.domain.devices.Switch;
import lv.artjomsfedosenko.sonoffcontrolltest.domain.devices.SwitchPosition;
import lv.artjomsfedosenko.sonoffcontrolltest.domain.devices.SwitchState;
import lv.artjomsfedosenko.sonoffcontrolltest.services.DeviceRegistry;
import lv.artjomsfedosenko.sonoffcontrolltest.services.devicestate.DeviceStateResolver;
import org.junit.Before;
import org.junit.Test;

import java.nio.charset.Charset;
import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

public class DeviceStateRouterTest {

    private DeviceStateRouter router;

    private DeviceRegistry registry;

    private Switch device;

    private DeviceStateResolver resolver;

    @Before
    public void setUp() throws Exception {

        Switch switchDevice = this.createTestDeice();
        //Map<String, Integer> deviceMap =
        DeviceRegistry registry = mock(DeviceRegistry.class);
        when(registry.getDeviceById(1)).thenReturn(switchDevice);

        DeviceStateResolver resolver = new DeviceStateResolver();

        DeviceStateRouter router = new DeviceStateRouter(registry, resolver);
        router.addRoute("switchStateTopic", 1);

        this.registry = registry;
        this.router = router;
        this.device = switchDevice;
        this.resolver = resolver;
    }

    @Test
    public void onPublishChangesStateOfCorrectDevice() {

        ByteBuf msgResponse = Unpooled.wrappedBuffer("ON".getBytes(Charset.forName("UTF-8")));
        InterceptPublishMessage msg = mock(InterceptPublishMessage.class);
        when(msg.getPayload()).thenReturn(msgResponse);
        when(msg.getTopicName()).thenReturn("switchStateTopic");

        assertEquals(SwitchState.OFF, this.device.getSwitchState());

        this.router.onPublish(msg);

        verify(this.registry).getDeviceById(1);
        assertEquals(SwitchState.ON, this.device.getSwitchState());
    }

    @Test
    public void addRoute() {
    }

    private Switch createTestDeice() {
        Switch switchDevice = new Switch();
        switchDevice.setId(1)
                .setName("switch")
                .setActionTopic("switchActionTopic")
                .setStateTopic("switchStateTopic")
                .setTelemetryTopic("switchTelemetryTopic")
                .setModuleId("switchModuleId")
                .setDeviceStatus(DeviceStatus.ONLINE);
        switchDevice.setSwitchState(SwitchState.OFF)
                .setSwitchPosition(SwitchPosition.ON);

        return switchDevice;
    }
}
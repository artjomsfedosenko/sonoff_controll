package lv.artjomsfedosenko.sonoffcontrolltest.mqtt;

import io.moquette.interception.messages.*;
import lv.artjomsfedosenko.sonoffcontrolltest.domain.Module;
import lv.artjomsfedosenko.sonoffcontrolltest.domain.ModuleStatus;
import lv.artjomsfedosenko.sonoffcontrolltest.events.ModuleStatusChangeEvent;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationEventPublisher;

import java.lang.reflect.Field;
import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;


public class ModuleWatcherTest {

    private ApplicationEventPublisher eventPublisher;

    private ModuleWatcher moduleWatcher;

    @Before
    public void setUp() throws Exception {

        ApplicationEventPublisher eventPublisher = mock(ApplicationEventPublisher.class);

        //this.moduleWatcher = spy(new ModuleWatcher());
        ModuleWatcher moduleWatcher = new ModuleWatcher(eventPublisher);

        Module module = new Module("existingModule", ModuleStatus.CONNECTED);
        module.addSubscribedTopic("existingTopic");
        Module moduleToDisconnect = new Module("existingModuleToDisconnect", ModuleStatus.CONNECTED);
        Module moduleToLoseConnection = new Module("existingModuleToLoseConnection", ModuleStatus.CONNECTED);
        Module moduleLostConnection = new Module("existingModuleLostConnection", ModuleStatus.CONNECTION_LOST);

        Field connectedModulesField = moduleWatcher.getClass().getDeclaredField("modules");
        connectedModulesField.setAccessible(true);
        Map<String, Module> connectedModules = (Map<String, Module>) connectedModulesField.get(moduleWatcher);
        connectedModules.put("existingModule", module);
        connectedModules.put("existingModuleToDisconnect", moduleToDisconnect);
        connectedModules.put("existingModuleToLoseConnection", moduleToLoseConnection);
        connectedModules.put("existingModuleLostConnection", moduleLostConnection);

        this.eventPublisher = eventPublisher;
        this.moduleWatcher = moduleWatcher;
    }

    @Test
    public void testOnConnectAddsModule() {

        InterceptConnectMessage connectMessage = mock(InterceptConnectMessage.class);
        when(connectMessage.getClientID()).thenReturn("testClient");

        this.moduleWatcher.onConnect(connectMessage);

        Map<String, Module> modules = this.moduleWatcher.getModules();
        Module addedModule = modules.get("testClient");

        //TODO: find a way to test it properly
        verify(this.eventPublisher).publishEvent(any(ModuleStatusChangeEvent.class));
        assertNotNull(addedModule);
        assertEquals("testClient", addedModule.getModuleId());
    }

    @Test
    public void testOnDisconnectSetsModuleDisconnected() {

        InterceptDisconnectMessage disconnectMessage = mock(InterceptDisconnectMessage.class);
        when(disconnectMessage.getClientID()).thenReturn("existingModuleToDisconnect");

        Map<String, Module> connectedModules = this.moduleWatcher.getModules();
        Module module = connectedModules.get("existingModuleToDisconnect");

        assertNotNull(module);
        assertEquals(ModuleStatus.CONNECTED, module.getModuleStatus());
        this.moduleWatcher.onDisconnect(disconnectMessage);
        verify(this.eventPublisher).publishEvent(any(ModuleStatusChangeEvent.class));
        assertEquals(ModuleStatus.DISCONNECTED, module.getModuleStatus());
    }

    @Test
    public void testOnSubscribeAddsTopicToModule() {

        InterceptSubscribeMessage subscribeMessage = mock(InterceptSubscribeMessage.class);
        when(subscribeMessage.getClientID()).thenReturn("existingModule");
        when(subscribeMessage.getTopicFilter()).thenReturn("newTopic");

        this.moduleWatcher.onSubscribe(subscribeMessage);
        Module module = this.moduleWatcher.getModules().get("existingModule");
        assertTrue(module.getSubscribedTopics().contains("newTopic"));
    }

    @Test
    public void onUnsubscribeRemovesSubscribedTopic() {

        InterceptUnsubscribeMessage unsubscribeMessage = mock(InterceptUnsubscribeMessage.class);
        when(unsubscribeMessage.getClientID()).thenReturn("existingModule");
        when(unsubscribeMessage.getTopicFilter()).thenReturn("existingTopic");

        Module module = this.moduleWatcher.getModules().get("existingModule");
        assertTrue(module.getSubscribedTopics().contains("existingTopic"));
        this.moduleWatcher.onUnsubscribe(unsubscribeMessage);
        assertFalse(module.getSubscribedTopics().contains("existingTopic"));
    }

    @Test
    public void onConnectionLostSetsModuleStateConnectionLost() {

        InterceptConnectionLostMessage connectionLostMessage = mock(InterceptConnectionLostMessage.class);
        when(connectionLostMessage.getClientID()).thenReturn("existingModuleToLoseConnection");

        Map<String, Module> connectedModules = this.moduleWatcher.getModules();
        Module module = connectedModules.get("existingModuleToLoseConnection");

        assertNotNull(module);
        assertEquals(ModuleStatus.CONNECTED, module.getModuleStatus());
        this.moduleWatcher.onConnectionLost(connectionLostMessage);
        verify(this.eventPublisher).publishEvent(any(ModuleStatusChangeEvent.class));
        assertEquals(ModuleStatus.CONNECTION_LOST, module.getModuleStatus());
    }

    @Test
    public void onConnectRenewsDisconnectedModule() {
        //existingModuleLostConnection
        InterceptConnectMessage connectMessage = mock(InterceptConnectMessage.class);
        when(connectMessage.getClientID()).thenReturn("existingModuleLostConnection");

        Module module = this.moduleWatcher.getModules().get("existingModuleLostConnection");

        assertEquals(ModuleStatus.CONNECTION_LOST, module.getModuleStatus());
        this.moduleWatcher.onConnect(connectMessage);
        verify(this.eventPublisher).publishEvent(any(ModuleStatusChangeEvent.class));
        assertEquals(ModuleStatus.CONNECTED, module.getModuleStatus());
    }
}
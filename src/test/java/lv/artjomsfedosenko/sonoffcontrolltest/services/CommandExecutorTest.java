package lv.artjomsfedosenko.sonoffcontrolltest.services;

import lv.artjomsfedosenko.sonoffcontrolltest.commands.CommandHandlerInterface;
import lv.artjomsfedosenko.sonoffcontrolltest.commands.CommandInterface;
import lv.artjomsfedosenko.sonoffcontrolltest.commands.CommandResultInterface;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class CommandExecutorTest {

    private CommandExecutor executor;
    private CommandHandlerInterface testHandler;
    private CommandInterface testCommand;

    class TestCommand implements CommandInterface {}
    class TestResult implements CommandResultInterface {}

    @Before
    public void setUp() throws Exception {

        TestCommand testCommand = new TestCommand();

        CommandHandlerInterface<TestCommand, TestResult> testHandler =
                mock(CommandHandlerInterface.class);
        when(testHandler.execute(testCommand)).thenReturn(new TestResult());
        when(testHandler.getCommandType()).thenReturn(TestCommand.class);

        List<CommandHandlerInterface> handlers = new ArrayList<>();
        handlers.add(testHandler);
        CommandExecutor executor = new CommandExecutor(handlers);

        this.testHandler = testHandler;
        this.executor = executor;
        this.testCommand = testCommand;
    }

    @Test
    public void testExecuteCallsHandlerAndReturnsResult() throws Exception {

        TestResult result = this.executor.execute(this.testCommand);

        verify(this.testHandler, atLeastOnce()).execute(this.testCommand);
        assertNotNull(result);
        assertEquals(TestResult.class, result.getClass());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testExecuteWithIncorrectArgumentThrowsException() throws Exception {

        this.executor.execute(new CommandInterface() {});

    }
}
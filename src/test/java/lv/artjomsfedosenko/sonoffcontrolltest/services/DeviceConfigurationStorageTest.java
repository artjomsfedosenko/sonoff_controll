package lv.artjomsfedosenko.sonoffcontrolltest.services;

import lv.artjomsfedosenko.sonoffcontrolltest.domain.DeviceConfiguration;
import lv.artjomsfedosenko.sonoffcontrolltest.domain.DeviceTypes;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileWriter;
import java.util.List;

import static org.junit.Assert.*;

public class DeviceConfigurationStorageTest {

    private DeviceConfigurationStorage storage;

    @Before
    public void setUp() throws Exception {

        DeviceConfigurationStorage storage = new DeviceConfigurationStorage("testDevices");
        this.storage = storage;

        this.createStoredConfig("{\n" +
                "  \"id\": 1,\n" +
                "  \"name\": \"device1\",\n" +
                "  \"actionTopic\": \"actionTopic1\",\n" +
                "  \"stateTopic\": \"stateTopic1\",\n" +
                "  \"telemetryTopic\": \"teleTopic1\",\n" +
                "  \"deviceType\": \"SWITCH\",\n" +
                "  \"moduleId\": \"module1\"\n" +
                "}", "1");

        this.createStoredConfig("{\n" +
                "  \"id\": 2,\n" +
                "  \"name\": \"device2\",\n" +
                "  \"actionTopic\": \"actionTopic2\",\n" +
                "  \"stateTopic\": \"stateTopic2\",\n" +
                "  \"telemetryTopic\": \"teleTopic2\",\n" +
                "  \"deviceType\": \"TEMPERATURE_SENSOR\",\n" +
                "  \"moduleId\": \"module2\"\n" +
                "}", "2");
    }

    private void createStoredConfig(String device, String id) throws Exception {

        File deviceFile = new File("testDevices/" + id + ".json");

        try (FileWriter fileWriter = new FileWriter(deviceFile)) {
            fileWriter.write(device);
        } catch (Exception e) {
            throw e;
        }
    }

    @After
    public void tearDown() throws Exception {

        File testFolder = new File("testDevices");
        File[] files = testFolder.listFiles();

        if (files != null) {
            for (File file : files) {
                file.delete();
            }
        }

        testFolder.delete();
    }

    @Test
    public void testSaveReturnsExpectedId() throws Exception {

        DeviceConfiguration configuration = new DeviceConfiguration()
                .setName("testDevice")
                .setActionTopic("actionTopic")
                .setStateTopic("stateTopic")
                //.setStatusTopic("statusTopic")
                .setTelemetryTopic("teleTopic")
                .setDeviceType(DeviceTypes.SWITCH)
                .setModuleId("moduleId");

        Integer deviceId = this.storage.save(configuration);

        assertEquals(Integer.valueOf(3), deviceId);
    }

    @Test
    public void testGetDeviceConfigurationsReturnsSavedConfigs() throws Exception {

        List<DeviceConfiguration> configurations = this.storage.getDeviceConfigurations();

        assertNotNull(configurations);
        assertTrue(configurations.size() == 2);

        DeviceConfiguration device1 = configurations.get(0);
        assertEquals(Integer.valueOf(1), device1.getId());
        assertEquals("device1", device1.getName());
        assertEquals("actionTopic1", device1.getActionTopic());
        //assertEquals("statusTopic1", device1.getStatusTopic());
        assertEquals("stateTopic1", device1.getStateTopic());
        assertEquals("teleTopic1", device1.getTelemetryTopic());
        assertEquals(DeviceTypes.SWITCH, device1.getDeviceType());
        assertEquals("module1", device1.getModuleId());

        DeviceConfiguration device2 = configurations.get(1);
        assertEquals(Integer.valueOf(2), device2.getId());
        assertEquals("device2", device2.getName());
        assertEquals("actionTopic2", device2.getActionTopic());
        //assertEquals("statusTopic2", device2.getStatusTopic());
        assertEquals("stateTopic2", device2.getStateTopic());
        assertEquals("teleTopic2", device2.getTelemetryTopic());
        assertEquals(DeviceTypes.TEMPERATURE_SENSOR, device2.getDeviceType());
        assertEquals("module2", device2.getModuleId());

    }

}

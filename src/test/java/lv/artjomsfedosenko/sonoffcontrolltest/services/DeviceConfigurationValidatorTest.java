package lv.artjomsfedosenko.sonoffcontrolltest.services;

import lv.artjomsfedosenko.sonoffcontrolltest.domain.DeviceConfiguration;
import lv.artjomsfedosenko.sonoffcontrolltest.domain.DeviceTypes;
import lv.artjomsfedosenko.sonoffcontrolltest.domain.Module;
import lv.artjomsfedosenko.sonoffcontrolltest.mqtt.ModuleWatcher;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.hamcrest.Matchers.*;

public class DeviceConfigurationValidatorTest {

    private DeviceConfigurationValidator validator;

    private List<DeviceConfiguration> configList = new ArrayList<>();

    private Map<String, Module> modules = new HashMap<>();

    private DeviceConfiguration configForTest;

    @Before
    public void setUp() throws Exception {

        //Set up ConfigStorage mock
        DeviceConfiguration configuration = new DeviceConfiguration()
                .setName("testDevice")
                .setActionTopic("existingActionTopic")
                .setStateTopic("existingStateTopic");
        this.configList.add(configuration);

        DeviceConfigurationStorage storage = mock(DeviceConfigurationStorage.class);
        when(storage.getDeviceConfigurations()).thenReturn(this.configList);

        //Set up ModuleWatcher
        this.modules.put("module1", null);
        this.modules.put("module2", null);

        ModuleWatcher moduleWatcher = mock(ModuleWatcher.class);
        when(moduleWatcher.getModules()).thenReturn(this.modules);

        DeviceConfigurationValidator validator = new DeviceConfigurationValidator(moduleWatcher, storage);
        this.validator = validator;

        //{repare config for testing
        DeviceConfiguration configForTest = new DeviceConfiguration()
                .setName("testDevice")
                .setActionTopic("actionTopic")
                .setStateTopic("stateTopic")
                //.setStatusTopic("statusTopic")
                .setTelemetryTopic("teleTopic")
                .setDeviceType(DeviceTypes.SWITCH)
                .setModuleId("moduleId");

        this.configForTest = configForTest;
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test(expected = NullPointerException.class)
    public void testValidateThrowsExceptionIfNameIsNull() {

        this.configForTest.setName(null);
        this.validator.validate(this.configForTest);
    }

    @Test
    public void testValidateThrowsExceptionIfNameIsEmpty() {

        this.configForTest.setName("");

        try {
            this.validator.validate(this.configForTest);
            fail("Expected an IllegalArgumentException to be thrown");
        } catch (IllegalArgumentException exception) {
            assertThat(exception.getMessage(), is("Device name should not be empty"));
        }
    }

    @Test
    public void testValidateThrowsExceptionIfTopicIsUsed() {

        this.configForTest.setActionTopic("existingActionTopic");

        try {
            this.validator.validate(this.configForTest);
            fail("Expected an IllegalArgumentException to be thrown");
        } catch (IllegalArgumentException exception) {
            assertThat(exception.getMessage(), is("Topic already in use by testDevice"));
        }
    }

    @Test
    public void testValidateThrowsExceptionIfModuleDoNotExist() {

        try {
            this.validator.validate(this.configForTest);
            fail("Expected an IllegalArgumentException to be thrown");
        } catch (IllegalArgumentException exception) {
            assertThat(exception.getMessage(), is("Module Id moduleId is invalid"));
        }
    }
}
package lv.artjomsfedosenko.sonoffcontrolltest.services;

import lv.artjomsfedosenko.sonoffcontrolltest.domain.DeviceConfiguration;
import lv.artjomsfedosenko.sonoffcontrolltest.domain.DeviceTypes;
import lv.artjomsfedosenko.sonoffcontrolltest.domain.devices.*;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class DeviceFactoryTest {

    private DeviceConfiguration configuration;

    private DeviceFactory factory;

    @Before
    public void setUp() throws Exception {

        DeviceConfiguration configuration = new DeviceConfiguration()
                .setId(1)
                .setName("testDevice")
                .setActionTopic("actionTopic")
                .setStateTopic("stateTopic")
                //.setStatusTopic("statusTopic")
                .setTelemetryTopic("teleTopic")
                .setModuleId("moduleId");

        this.configuration = configuration;
        this.factory = new DeviceFactory();
    }

    @Test
    public void createDeviceReturnsDeviceWithCorrectCommonParams() throws Exception {

        this.configuration.setDeviceType(DeviceTypes.SWITCH);
        Device device = this.factory.createDevice(this.configuration);

        assertEquals(Integer.valueOf(1), device.getId());
        assertEquals("testDevice", device.getName());
        assertEquals("actionTopic", device.getActionTopic());
        //assertEquals("statusTopic", device.getStatusTopic());
        assertEquals("stateTopic", device.getStateTopic());
        assertEquals("teleTopic", device.getTelemetryTopic());
        assertEquals("moduleId", device.getModuleId());
        assertEquals(DeviceStatus.OFFLINE, device.getDeviceStatus());
    }

    @Test
    public void createDeviceCreatesSwitch() throws Exception {
        this.configuration.setDeviceType(DeviceTypes.SWITCH);
        Device device = this.factory.createDevice(this.configuration);

        assertTrue(device instanceof Switch);
        Switch switchDevice = (Switch) device;
        assertEquals(SwitchPosition.OFF, switchDevice.getSwitchPosition());
        assertEquals(SwitchState.OFF, switchDevice.getSwitchState());
    }

    @Test
    public void createDeviceCreatesTemperatureSensor() throws Exception {
        this.configuration.setDeviceType(DeviceTypes.TEMPERATURE_SENSOR);
        Device device = this.factory.createDevice(this.configuration);

        assertTrue(device instanceof TemperatureSensor);
        TemperatureSensor sensor = (TemperatureSensor) device;
        assertNull(sensor.getTemperature());
    }

    @Test
    public void createDeviceCreatesTemperatureHumiditySensor() throws Exception {
        this.configuration.setDeviceType(DeviceTypes.TEMPERTATURE_HUMIDITY_SENSOR);
        Device device = this.factory.createDevice(this.configuration);

        assertTrue(device instanceof TemperatureHumiditySensor);
        TemperatureHumiditySensor sensor = (TemperatureHumiditySensor) device;
        assertNull(sensor.getTemperature());
        assertNull(sensor.getHumidity());
    }

    @Test(expected = Exception.class)
    public void createDeviceThrowsExceptionForDeviceWithUnknownType() throws Exception {
        this.configuration.setDeviceType(null);
        Device device = this.factory.createDevice(this.configuration);
    }

}
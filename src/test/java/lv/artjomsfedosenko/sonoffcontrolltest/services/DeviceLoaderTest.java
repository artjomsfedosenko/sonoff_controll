package lv.artjomsfedosenko.sonoffcontrolltest.services;

import lv.artjomsfedosenko.sonoffcontrolltest.domain.DeviceConfiguration;
import lv.artjomsfedosenko.sonoffcontrolltest.domain.devices.Device;
import lv.artjomsfedosenko.sonoffcontrolltest.events.DeviceAddedEvent;
import lv.artjomsfedosenko.sonoffcontrolltest.mqtt.DeviceStateRouter;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationEventPublisher;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class DeviceLoaderTest {

    private DeviceFactory deviceFactory;

    private DeviceRegistry deviceRegistry;

    private ApplicationEventPublisher eventPublisher;

    private DeviceLoader deviceLoader;

    private DeviceStateRouter router;

    @Before
    public void setUp() throws Exception {

        Device device = new Device().setId(123)
            .setActionTopic("actionTopic")
            .setStateTopic("stateTopic")
            .setTelemetryTopic("teleTopic");
        DeviceFactory deviceFactory = mock(DeviceFactory.class);
        when(deviceFactory.createDevice(any(DeviceConfiguration.class))).thenReturn(device);

        DeviceRegistry deviceRegistry = mock(DeviceRegistry.class);

        ApplicationEventPublisher eventPublisher = mock(ApplicationEventPublisher.class);

        DeviceStateRouter router = mock(DeviceStateRouter.class);

        this.deviceFactory = deviceFactory;
        this.deviceRegistry = deviceRegistry;
        this.eventPublisher = eventPublisher;
        this.router = router;
        this.deviceLoader = new DeviceLoader(
                eventPublisher, deviceFactory, deviceRegistry, router
        );
    }

    @Test
    public void testLoadDeviceCallsExpectedMethods() throws Exception {

        DeviceConfiguration configuration = new DeviceConfiguration()
                .setId(123);
        Device device = this.deviceLoader.loadDevice(configuration);

        //Verify factory called
        verify(this.deviceFactory).createDevice(any(DeviceConfiguration.class));

        //Verify device registry is called
        verify(this.deviceRegistry).addDevice(any(Device.class));

        //Verify device added to router
        verify(this.router).addRoute("stateTopic", 123);
        verify(this.router).addRoute("teleTopic", 123);

        //Verify, that event is triggered
        verify(this.eventPublisher).publishEvent(any(DeviceAddedEvent.class));

        assertEquals(Integer.valueOf(123), device.getId());
    }
}
package lv.artjomsfedosenko.sonoffcontrolltest.services;

import lv.artjomsfedosenko.sonoffcontrolltest.domain.ModuleStatus;
import lv.artjomsfedosenko.sonoffcontrolltest.domain.devices.DeviceStatus;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ModuleStatusConverterTest {

    private ModuleStatusConverter converter;

    @Before
    public void setUp() throws Exception {
        this.converter = new ModuleStatusConverter();
    }

    @Test
    public void testModuleConnectedConvertsToDeviceOnline() {

        DeviceStatus status = this.converter.getDeviceStatusByModuleStatus(ModuleStatus.CONNECTED);
        assertEquals(DeviceStatus.ONLINE, status);
    }

    @Test
    public void testModuleDisconnectedConvertsToDeviceOffline() {
        DeviceStatus status = this.converter.getDeviceStatusByModuleStatus(ModuleStatus.DISCONNECTED);
        assertEquals(DeviceStatus.OFFLINE, status);
    }

    @Test
    public void testModuleConnectionLostConvertsToDeviceOffline() {
        DeviceStatus status = this.converter.getDeviceStatusByModuleStatus(ModuleStatus.CONNECTION_LOST);
        assertEquals(DeviceStatus.OFFLINE, status);
    }
}
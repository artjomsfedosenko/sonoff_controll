package lv.artjomsfedosenko.sonoffcontrolltest.services.devicestate;

import lv.artjomsfedosenko.sonoffcontrolltest.domain.devices.*;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

//TODO: test for malformed states
public class DeviceStateResolverTest {

    private DeviceStateResolver resolver;

    @Before
    public void setUp() throws Exception {

        this.resolver = new DeviceStateResolver();
    }

    @Test
    public void resolveSwitchSetsExpectedSwitchState() {

        Device device = this.getTestSwitch();
        String messagePayload = "ON";

        this.resolver.resolve(device, messagePayload);

        Switch switchDevice = (Switch) device;
        assertEquals(SwitchState.ON, switchDevice.getSwitchState());

    }

    @Test
    public void resolveSwitchSetsExpectedSwitchTelemetryState() {

        Device device = this.getTestSwitch();
        String messagePayload = "{\"Time\":\"2018-07-22T11:14:53\",\"Uptime\":\"0T00:10:26\",\"Vcc\":3.227," +
                "\"POWER\":\"ON\",\"Wifi\":{\"AP\":1,\"SSId\":\"HUAWEI_nova_8044\",\"RSSI\":44," +
                "\"APMac\":\"00:5A:13:AC:37:F3\"}}";

        this.resolver.resolve(device, messagePayload);

        Switch switchDevice = (Switch) device;
        assertEquals(SwitchState.ON, switchDevice.getSwitchState());
    }

    @Test
    public void testResolveSwitchStateSetesCorrectSwitchPosition() {

        Switch switchDevice =  (Switch) this.getTestSwitch();
        switchDevice.setSwitchState(SwitchState.ON);
        String messagePayload = "OFF";

        this.resolver.resolve(switchDevice, messagePayload);

        assertEquals(SwitchPosition.OFF, switchDevice.getSwitchPosition());
    }

    @Test
    public void testResolveSwitchStateSetesCorrectSwitchPositionFromTelemetry() {

        Switch switchDevice =  (Switch) this.getTestSwitch();
        switchDevice.setSwitchState(SwitchState.ON);
        String messagePayload = "{\"Time\":\"2018-07-22T11:14:53\",\"Uptime\":\"0T00:10:26\",\"Vcc\":3.227," +
                "\"POWER\":\"OFF\",\"Wifi\":{\"AP\":1,\"SSId\":\"HUAWEI_nova_8044\",\"RSSI\":44," +
                "\"APMac\":\"00:5A:13:AC:37:F3\"}}";

        this.resolver.resolve(switchDevice, messagePayload);

        assertEquals(SwitchPosition.OFF, switchDevice.getSwitchPosition());
    }

    @Test
    public void resolveTemperatureSensorSetsExpectedTemperature() {
    }

    @Test
    public void resolveTemperatureAndHumiditySetsExpectedParams() {
        Device device = this.getTemperatureHumiditySensorDevice();
        String payload = "{\"StatusSNS\":{\"Time\":\"2018-07-22T11:09:53\",\"AM2301\":{\"Temperature\":30.1,\"Humidity\":52.6},\"TempUnit\":\"C\"}}";
        this.resolver.resolve(device, payload);

        TemperatureHumiditySensor sensor = (TemperatureHumiditySensor) device;
        assertEquals(Float.valueOf(30.1F), sensor.getTemperature());
        assertEquals(Float.valueOf(52.6F), sensor.getHumidity());
    }

    @Test
    public void resolveTemperatureAndHumiditySetsExpectedParamsFromTelemetry() {
        Device device = this.getTemperatureHumiditySensorDevice();
        String payload = "{\"Time\":\"2018-07-22T11:06:29\",\"AM2301\":{\"Temperature\":27.9,\"Humidity\":55.3},\"TempUnit\":\"C\"}";
        this.resolver.resolve(device, payload);

        TemperatureHumiditySensor sensor = (TemperatureHumiditySensor) device;
        assertEquals(Float.valueOf(27.9F), sensor.getTemperature());
        assertEquals(Float.valueOf(55.3F), sensor.getHumidity());
    }

    private Device getTestSwitch() {
        Switch switchDevice = new Switch();
        switchDevice.setId(1)
                .setName("switch")
                .setActionTopic("switchActionTopic")
                .setStateTopic("switchStateTopic")
                .setTelemetryTopic("switchTelemetryTopic")
                .setModuleId("switchModuleId")
                .setDeviceStatus(DeviceStatus.ONLINE);
        switchDevice.setSwitchState(SwitchState.OFF)
                .setSwitchPosition(SwitchPosition.ON);

        return switchDevice;
    }

    private Device getTemperatureSensorDevice() {
        TemperatureSensor sensorDevice = new TemperatureSensor();
        sensorDevice.setId(2)
                .setName("sensor")
                .setActionTopic("sensorActionTopic")
                .setStateTopic("sensorStateTopic")
                .setTelemetryTopic("sensorTelemetryTopic")
                .setModuleId("sensorModuleId")
                .setDeviceStatus(DeviceStatus.ONLINE);
        sensorDevice.setTemperature(21.5F);

        return sensorDevice;
    }

    private Device getTemperatureHumiditySensorDevice() {
        TemperatureHumiditySensor humidityDevice = new TemperatureHumiditySensor();
        humidityDevice.setId(3)
                .setName("humiditySensor")
                .setActionTopic("humidityActionTopic")
                .setStateTopic("humidityStateTopic")
                .setTelemetryTopic("humidityTelemetryTopic")
                .setModuleId("humidityModuleId")
                .setDeviceStatus(DeviceStatus.ONLINE);
        humidityDevice.setTemperature(23.5F)
                .setHumidity(65.5F);

        return humidityDevice;
    }
}